package service

import (
	"gitlab.com/farolina.r/hris-api-go/domain"
	"gitlab.com/farolina.r/hris-api-go/dto"
	"gitlab.com/farolina.r/hris-api-go/resp"
)

type MasterService interface {
	GetAttendanceLocations() ([]*dto.LocationAccess, *resp.AppError)
	GetAttendanceRadiusLocations(staffCode int64, coordinate dto.LocationCoordinates) (*dto.AttendanceLocationResponse, *resp.AppError)
}

type DefaultMasterService struct {
	repo domain.MasterRepository
}

func NewMasterService(repo domain.MasterRepository) *DefaultMasterService {
	return &DefaultMasterService{
		repo: repo,
	}
}

func (m *DefaultMasterService) GetAttendanceLocations() ([]*dto.LocationAccess, *resp.AppError) {
	locations, err := m.repo.GetAttendanceLocations()
	if err != nil {
		return nil, err
	}

	// convert every location access to dto
	locationAccessDto := make([]*dto.LocationAccess, len(locations))
	for i, job := range locations {
		locationAccessDto[i] = job.ToDTO()
	}

	return locationAccessDto, nil
}

func (m *DefaultMasterService) GetAttendanceRadiusLocations(staffCode int64, coordinate dto.LocationCoordinates) (*dto.AttendanceLocationResponse, *resp.AppError) {
	c := domain.LocationCoordinates{
		Latitude:  coordinate.Latitude,
		Longitude: coordinate.Longitude,
	}

	locations, err := m.repo.GetAttendanceRadiusLocations(staffCode, c)
	if err != nil {
		return nil, err
	}

	// convert every locations to dto
	locationsDto := make([]*dto.LocationAccess, len(locations))
	for i, job := range locations {
		locationsDto[i] = job.ToDTO()
	}

	return &dto.AttendanceLocationResponse{
		ListLocation: locationsDto,
	}, nil
}
