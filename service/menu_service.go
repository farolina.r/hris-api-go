package service

import (
	"gitlab.com/farolina.r/hris-api-go/domain"
	"gitlab.com/farolina.r/hris-api-go/dto"
	"gitlab.com/farolina.r/hris-api-go/resp"
)

type MenuService interface {
	GetMenuMobile(staffCode string) (*dto.DefaultMenuData, *resp.ErrorResponse)
	SetDefaultMenuMobile(staffCode, host string, menuData *dto.DefaultEmployeeMenuMobile) *resp.ErrorResponse
}

type DefaultMenuService struct {
	repo domain.MenuRepository
}

func (m *DefaultMenuService) SetDefaultMenuMobile(staffCode, host string, menuData *dto.DefaultEmployeeMenuMobile) *resp.ErrorResponse {
	d := domain.DefaultEmployeeMenuMobile{
		StaffCode: menuData.StaffCode,
		MenuID:    menuData.MenuID,
		SeqNo:     menuData.SeqNo,
		Notes:     menuData.Notes,
	}
	err := m.repo.SetDefaultMenuMobile(staffCode, host, &d)
	if err != nil {
		return err
	}
	return nil
}

func (m *DefaultMenuService) GetMenuMobile(staffCode string) (*dto.DefaultMenuData, *resp.ErrorResponse) {
	menu, err := m.repo.GetMenuMobile(staffCode)
	if err != nil {
		return nil, err
	}

	// convert menu.menuMobileTree to dto.MenuTree
	menuTreesDto := make([]*dto.MenuTree, len(menu.MenuMobileTree))
	for i, tree := range menu.MenuMobileTree {
		menuTreesDto[i] = tree.ToDTO()
	}

	// convert menu.menuMobile to dto.MenuMobile
	menuMobilesDto := make([]*dto.MenuMobile, len(menu.MenuMobile))
	for i, menuMobile := range menu.MenuMobile {
		menuMobilesDto[i] = menuMobile.ToDTO()
	}

	menuDto := &dto.DefaultMenuData{
		MenuMobile:     menuMobilesDto,
		MenuMobileTree: menuTreesDto,
	}

	return menuDto, nil
}

func NewMenuService(repo domain.MenuRepository) MenuService {
	return &DefaultMenuService{
		repo: repo,
	}
}
