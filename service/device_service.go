package service

import (
	"gitlab.com/farolina.r/hris-api-go/domain"
	"gitlab.com/farolina.r/hris-api-go/dto"
	"gitlab.com/farolina.r/hris-api-go/resp"
)

type DeviceService interface {
	SetDevicePIN(staffCode int64, devicePIN dto.DevicePINRequest, host string) *resp.ErrorResponse
	RemoveDevice(request dto.RemoveDeviceRequest, staffCode string, host string) *resp.ErrorResponse
}

type DefaultDeviceService struct {
	repo domain.DeviceRepository
}

func (e *DefaultDeviceService) RemoveDevice(request dto.RemoveDeviceRequest, staffCode string, host string) *resp.ErrorResponse {
	d := domain.RemoveDeviceRequest{
		DeviceID:      request.DeviceID,
		StaffCode:     request.StaffCode,
		ReasonDeleted: request.ReasonDeleted,
	}

	return e.repo.RemoveDevice(d, staffCode, host)
}

func (e *DefaultDeviceService) SetDevicePIN(staffCode int64, devicePIN dto.DevicePINRequest, host string) *resp.ErrorResponse {
	// dto to domain
	d := domain.DevicePIN{
		DeviceID:    devicePIN.DeviceID,
		PIN:         devicePIN.PIN,
		Token:       devicePIN.Token,
		ModelDevice: devicePIN.ModelDevice,
		OSDevice:    devicePIN.OSDevice,
	}

	return e.repo.SetDevicePIN(staffCode, d, host)
}

func NewDeviceService(repo domain.DeviceRepository) DeviceService {
	return &DefaultDeviceService{
		repo: repo,
	}
}
