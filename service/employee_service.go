package service

import (
	"gitlab.com/farolina.r/hris-api-go/domain"
	"gitlab.com/farolina.r/hris-api-go/dto"
	"gitlab.com/farolina.r/hris-api-go/logger"
	"gitlab.com/farolina.r/hris-api-go/resp"
)

type EmployeeService interface {
	GetEmployeePersonalInfo(staffCode int64) (*dto.EmployeePersonalInfo, *resp.ErrorResponse)
	GetEmployeesPersonalInfoBySearchKey(searchKey string) (*dto.EmployeePersonalInfo, *resp.ErrorResponse)
}

type DefaultEmployeeService struct {
	repo domain.EmployeeRepository
}

func (e *DefaultEmployeeService) getEmployeePersonalInfoDetails(staffCode int64) (*dto.EmployeePersonalInfo, *resp.ErrorResponse) {
	employeeDefaultMenu, err := e.repo.GetEmployeeDefaultMenuByStaffCode(staffCode)
	if err != nil {
		return nil, err
	}
	// convert every employee default menu to dto
	employeeDefaultMenuDto := make([]*dto.EmployeeMenuMobile, len(employeeDefaultMenu))
	for i, menu := range employeeDefaultMenu {
		employeeDefaultMenuDto[i] = menu.ToDTO()
	}

	employeeSchedule := make([]*dto.EmployeeSchedule, 0)

	employeeFamilies, err := e.repo.GetEmployeeFamilyByStaffCode(staffCode)
	if err != nil {
		return nil, err
	}
	// convert every employee families to dto
	employeeFamilyDto := make([]*dto.EmployeeFamily, len(employeeFamilies))
	for i, family := range employeeFamilies {
		employeeFamilyDto[i] = family.ToDTO()
	}

	employeeSecondaryJobs, err := e.repo.GetEmployeeSecondaryJobByStaffCode(staffCode)
	if err != nil {
		return nil, err
	}
	// convert every employee families to dto
	employeeSecondaryJobsDto := make([]*dto.EmployeeSecondaryJob, len(employeeSecondaryJobs))
	for i, job := range employeeSecondaryJobs {
		employeeSecondaryJobsDto[i] = job.ToDTO()
	}

	var employeePersonalInfo dto.EmployeePersonalInfo
	employeePersonalInfo.Schedule = employeeSchedule
	employeePersonalInfo.DefaultMenu = employeeDefaultMenuDto
	employeePersonalInfo.EmployeeFamily = employeeFamilyDto
	employeePersonalInfo.EmployeeSecondaryJob = employeeSecondaryJobsDto

	return &employeePersonalInfo, nil
}

func (e *DefaultEmployeeService) GetEmployeesPersonalInfoBySearchKey(searchKey string) (*dto.EmployeePersonalInfo, *resp.ErrorResponse) {
	employeesPersonalData, err := e.repo.GetEmployeesPersonalDataBySearchKey(searchKey)
	if err != nil {
		return nil, err
	}

	employeesPersonalDataDto := make([]*dto.EmployeePersonalData, len(employeesPersonalData))
	for i, data := range employeesPersonalData {
		employeesPersonalDataDto[i] = data.ToDTO()
	}

	var employeePersonalInfo *dto.EmployeePersonalInfo
	if len(employeesPersonalDataDto) == 1 {
		employeePersonalInfo, err = e.getEmployeePersonalInfoDetails(employeesPersonalData[0].StaffCode)
		if err != nil {
			logger.Error(err.Message)
		}
	}
	employeePersonalInfo.Employee = employeesPersonalDataDto

	return employeePersonalInfo, nil
}

func (e *DefaultEmployeeService) GetEmployeePersonalInfo(staffCode int64) (*dto.EmployeePersonalInfo, *resp.ErrorResponse) {
	employeePersonalData, err := e.repo.GetEmployeePersonalDataByStaffCode(staffCode)
	if err != nil {
		return nil, err
	}

	employeePersonalDataDto := make([]*dto.EmployeePersonalData, len(employeePersonalData))
	for i, job := range employeePersonalData {
		employeePersonalDataDto[i] = job.ToDTO()
	}

	var employeePersonalInfo *dto.EmployeePersonalInfo
	if len(employeePersonalDataDto) == 1 {
		employeePersonalInfo, err = e.getEmployeePersonalInfoDetails(staffCode)
		if err != nil {
			logger.Error(err.Message)
		}
	}
	employeePersonalInfo.Employee = employeePersonalDataDto

	return employeePersonalInfo, nil
}

func NewEmployeeService(repo domain.EmployeeRepository) EmployeeService {
	return &DefaultEmployeeService{
		repo: repo,
	}
}
