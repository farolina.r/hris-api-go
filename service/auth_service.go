package service

import (
	"gitlab.com/farolina.r/hris-api-go/domain"
	"gitlab.com/farolina.r/hris-api-go/dto"
	"gitlab.com/farolina.r/hris-api-go/logger"
	"gitlab.com/farolina.r/hris-api-go/resp"
	"net/http"
)

type AuthService interface {
	Login(*dto.LoginRequest, string) *resp.APIResponse
	Logout(session *dto.AppTokenSession) (*resp.APIResponse, *resp.ErrorResponse)
	RefreshToken(string, string, string) (*resp.APIResponse, *resp.ErrorResponse)
}

type DefaultAuthService struct {
	store domain.AuthStore
	repo  domain.AuthRepository
}

func (das *DefaultAuthService) Login(lr *dto.LoginRequest, host string) *resp.APIResponse {
	// validate LoginRequest
	errResp := lr.Validate()
	if errResp != nil {
		return errResp.ToAPIResponse()
	}

	request := domain.Login{
		Username: lr.Username,
		Password: lr.Password,
		Token:    lr.Token,
	}

	content, appErr := das.repo.Login(request, host)
	if appErr != nil {
		return resp.NewAPIResponse(appErr.Code, false, appErr.Message, nil)
	}

	// save tokens to store
	if das.store != nil {
		refreshKey := das.store.GetRefreshTokenKey(content.JWTToken)
		err := das.store.SetKey(refreshKey, content.RefreshToken)
		if err != nil {
			logger.Error(err.Error())
		}
	}

	return resp.NewAPIResponse(http.StatusOK, true, "", content)
}

func (das *DefaultAuthService) Logout(session *dto.AppTokenSession) (*resp.APIResponse, *resp.ErrorResponse) {
	sessionDomain := domain.AppTokenSession{
		StaffCode:      session.StaffCode,
		BlackpineToken: session.BlackpineToken,
		ExpiredAt:      session.ExpiredAt,
		CreatedBy:      session.CreatedBy,
		CreatedHost:    session.CreatedHost,
	}

	return das.repo.Logout(sessionDomain)
}

func (das *DefaultAuthService) RefreshToken(accessToken string, refreshToken string, host string) (*resp.APIResponse, *resp.ErrorResponse) {
	authStore := das.store
	if authStore == nil {
		tokens, err := das.repo.RefreshToken(accessToken, refreshToken, host)
		if err != nil {
			return nil, err
		}

		response := resp.NewAPIResponse(http.StatusOK, true, "", tokens)

		return response, nil
	}

	return das.store.RefreshToken(accessToken, refreshToken, host)

}

func NewAuthService(store domain.AuthStore, repo domain.AuthRepository) AuthService {
	return &DefaultAuthService{
		store: store,
		repo:  repo,
	}
}
