package middleware

import (
	"context"
	"gitlab.com/farolina.r/hris-api-go/domain"
	"gitlab.com/farolina.r/hris-api-go/helper"
	"gitlab.com/farolina.r/hris-api-go/logger"
	"gitlab.com/farolina.r/hris-api-go/resp"
	"net/http"
	"strings"
)

type AuthMiddleware struct {
	repo domain.AuthRepository
}

func (a AuthMiddleware) AuthorizationHandler() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			token := r.Header.Get("Authorization")
			if token == "" {
				w.WriteHeader(http.StatusUnauthorized)
				resp.WriteResponse(w, resp.UnauthorizedError("").ToAPIResponse())
				return
			}

			// split token from Bearer
			token = strings.Split(token, " ")[1]

			// verify jwt token
			claims, err := domain.AuthClaims{Token: token}.Verify()
			if err != nil {
				resp.WriteResponse(w, err.ToAPIResponse())
				return
			}

			user, err := a.repo.GetUserSessionByStaffCodeAndJWTToken(claims.StaffCode, token)
			if err != nil || user == nil {
				logger.Error(err.Message)
				resp.WriteResponse(w, resp.UnauthorizedError("").ToAPIResponse())
				return
			}

			// user already logged out
			if helper.ConvertTimeUTCToString(user.ExpiredAt) < helper.GetCurrentTimeUTC() {
				resp.WriteResponse(w, resp.UnauthorizedError(resp.ErrLoggedOut.Error()).ToAPIResponse())
				return
			}

			if user.StaffCode == 0 {
				resp.WriteResponse(w, resp.NewAPIResponse(
					http.StatusUnauthorized,
					false,
					"User not found",
					nil,
				))
				return
			}

			// set user.staffcode to context
			ctx := r.Context()
			ctx = context.WithValue(ctx, "user", user.ToDTO())
			r = r.WithContext(ctx)
			next.ServeHTTP(w, r)
		})
	}
}

func NewAuthMiddleware(repo domain.AuthRepository) AuthMiddleware {
	return AuthMiddleware{repo}
}
