package handlers

import (
	"gitlab.com/farolina.r/hris-api-go/dto"
	"gitlab.com/farolina.r/hris-api-go/helper"
	"gitlab.com/farolina.r/hris-api-go/logger"
	"gitlab.com/farolina.r/hris-api-go/resp"
	"gitlab.com/farolina.r/hris-api-go/service"
	"io"
	"net/http"
	"strings"
)

type AuthHandler struct {
	service service.AuthService
}

func (h *AuthHandler) Login(w http.ResponseWriter, r *http.Request) {
	var request dto.LoginRequest
	err := helper.DecodeReqBody(w, r, &request)
	if err != nil {
		if err == io.EOF {
			resp.WriteResponse(w, resp.ReqBodyEmptyError().ToAPIResponse())
		} else {
			logger.Error(err.Error())
			resp.WriteResponse(w, resp.ReqBodyInvalidError().ToAPIResponse())
		}
		return
	}

	host := helper.GetIP(r)

	request.Token = r.Header.Get("dont-panic")

	login := h.service.Login(&request, host)

	resp.WriteResponse(w, login)
}

func (h *AuthHandler) Logout(w http.ResponseWriter, r *http.Request) {
	session, err := helper.GetAppTokenSessionFromContext(r)
	if err != nil {
		resp.WriteResponse(w, resp.UnauthorizedError("").ToAPIResponse())
		return
	}

	logout, appErr := h.service.Logout(session)
	if appErr != nil {
		resp.WriteResponse(w, appErr.ToAPIResponse())
		return
	}

	resp.WriteResponse(w, logout)
}

func (h *AuthHandler) RefreshToken(w http.ResponseWriter, r *http.Request) {
	var request dto.RefreshTokenRequest
	err := helper.DecodeReqBody(w, r, &request)
	if err != nil {
		if err == io.EOF {
			resp.WriteResponse(w, resp.ReqBodyEmptyError().ToAPIResponse())
		} else {
			logger.Error(err.Error())
			resp.WriteResponse(w, resp.ReqBodyInvalidError().ToAPIResponse())
		}
		return
	}

	lr := &request
	appErr := lr.Validate()
	if appErr != nil {
		resp.WriteResponse(w, appErr.ToAPIResponse())
		return
	}

	host := helper.GetIP(r)

	accessToken := r.Header.Get("Authorization")
	if accessToken == "" {
		resp.WriteResponse(w, resp.UnauthorizedError("").ToAPIResponse())
		return
	}

	// split accessToken from Bearer
	accessToken = strings.Split(accessToken, " ")[1]

	response, appErr := h.service.RefreshToken(accessToken, request.RefreshToken, host)
	if appErr != nil {
		resp.WriteResponse(w, appErr.ToAPIResponse())
		return
	}

	resp.WriteResponse(w, response)
}

func NewAuthHandler(service service.AuthService) *AuthHandler {
	return &AuthHandler{service: service}
}
