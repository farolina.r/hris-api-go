package handlers

import (
	"gitlab.com/farolina.r/hris-api-go/dto"
	"gitlab.com/farolina.r/hris-api-go/helper"
	"gitlab.com/farolina.r/hris-api-go/logger"
	"gitlab.com/farolina.r/hris-api-go/resp"
	"gitlab.com/farolina.r/hris-api-go/service"
	"io"
	"net/http"
)

type EmployeeHandler struct {
	service service.EmployeeService
}

func (h *EmployeeHandler) getLoggedInEmployeePersonalInfo(staffCode int64) *resp.APIResponse {
	info, appErr := h.service.GetEmployeePersonalInfo(staffCode)
	if appErr != nil {
		return appErr.ToAPIResponse()
	}

	response := resp.NewAPIResponse(http.StatusOK, true, "", info)
	return response
}

func (h *EmployeeHandler) getSearchKeyEmployeePersonalInfo(searchKey string) *resp.APIResponse {
	info, appErr := h.service.GetEmployeesPersonalInfoBySearchKey(searchKey)
	if appErr != nil {
		return appErr.ToAPIResponse()
	}

	response := resp.NewAPIResponse(http.StatusOK, true, "", info)
	return response
}

func (h *EmployeeHandler) GetEmployeePersonalInfo(w http.ResponseWriter, r *http.Request) {
	staffCode, err := helper.GetStaffCodeFromContext(r)
	if err != nil {
		resp.WriteResponse(w, resp.UnauthorizedError("").ToAPIResponse())
		return
	}

	var response *resp.APIResponse

	// check request body
	var request dto.EmployeesPersonalInfoRequest
	err = helper.DecodeReqBody(w, r, &request)
	if err != nil {
		if err == io.EOF { // get logged in employee data if request body is empty
			response = h.getLoggedInEmployeePersonalInfo(staffCode)
		} else {
			logger.Error(err.Error())
			resp.WriteResponse(w, resp.ReqBodyInvalidError().ToAPIResponse())
			return
		}
	} else {
		if request.SearchKey != "" {
			response = h.getSearchKeyEmployeePersonalInfo(request.SearchKey)
		} else { // get logged in employee data if no search key
			response = h.getLoggedInEmployeePersonalInfo(staffCode)
		}
	}

	resp.WriteResponse(w, response)

}

func NewEmployeeHandler(service service.EmployeeService) *EmployeeHandler {
	return &EmployeeHandler{service: service}
}
