package handlers

import (
	"gitlab.com/farolina.r/hris-api-go/dto"
	"gitlab.com/farolina.r/hris-api-go/helper"
	"gitlab.com/farolina.r/hris-api-go/logger"
	"gitlab.com/farolina.r/hris-api-go/resp"
	"gitlab.com/farolina.r/hris-api-go/service"
	"io"
	"net/http"
)

type MasterHandler struct {
	service service.MasterService
}

func NewMasterHandler(service service.MasterService) *MasterHandler {
	return &MasterHandler{service: service}
}

func (m *MasterHandler) GetAttendanceLocations(w http.ResponseWriter, r *http.Request) {
	locations, err := m.service.GetAttendanceLocations()
	if err != nil {
		resp.WriteResponse(w, resp.NewAPIResponse(err.Code, false, err.Message, nil))
		return
	}

	resp.WriteResponse(w, resp.NewAPIResponse(http.StatusOK, true, "", locations))
}

func (m *MasterHandler) GetAttendanceRadiusLocations(w http.ResponseWriter, r *http.Request) {
	staffCode, err := helper.GetStaffCodeFromContext(r)
	if err != nil {
		resp.WriteResponse(w, resp.UnauthorizedError("").ToAPIResponse())
		return
	}

	// get coordinates from request body
	var coordinate dto.LocationCoordinates
	err = helper.DecodeReqBody(w, r, &coordinate)
	if err != nil {
		if err == io.EOF {
			resp.WriteResponse(w, resp.ReqBodyEmptyError().ToAPIResponse())
		} else {
			logger.Error(err.Error())
			resp.WriteResponse(w, resp.ReqBodyInvalidError().ToAPIResponse())
		}
		return
	}

	validation := coordinate.Validate()
	if validation != nil {
		resp.WriteResponse(w, resp.NewAPIResponse(validation.Code, false, validation.Message, nil))
		return
	}

	locations, appErr := m.service.GetAttendanceRadiusLocations(staffCode, coordinate)
	if appErr != nil {
		resp.WriteResponse(w, resp.NewAPIResponse(appErr.Code, false, appErr.Message, nil))
		return
	}

	resp.WriteResponse(w, resp.NewAPIResponse(http.StatusOK, true, "", locations))
}
