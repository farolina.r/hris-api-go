package handlers

import (
	"gitlab.com/farolina.r/hris-api-go/dto"
	"gitlab.com/farolina.r/hris-api-go/helper"
	"gitlab.com/farolina.r/hris-api-go/logger"
	"gitlab.com/farolina.r/hris-api-go/resp"
	"gitlab.com/farolina.r/hris-api-go/service"
	"io"
	"net/http"
	"strconv"
)

const SuccessSetDevicePin = "You have successfully Set PIN"
const SuccessRemoveDevice = "You have successfully Remove Device"

type DeviceHandler struct {
	service service.DeviceService
}

func (h *DeviceHandler) SetDevicePIN(w http.ResponseWriter, r *http.Request) {
	staffCode, err := helper.GetStaffCodeFromContext(r)
	if err != nil {
		resp.WriteResponse(w, resp.UnauthorizedError("").ToAPIResponse())
		return
	}

	// get device pin from request body
	var devicePIN dto.DevicePINRequest
	err = helper.DecodeReqBody(w, r, &devicePIN)
	if err != nil {
		if err == io.EOF {
			resp.WriteResponse(w, resp.ReqBodyEmptyError().ToAPIResponse())
		} else {
			logger.Error(err.Error())
			resp.WriteResponse(w, resp.ReqBodyInvalidError().ToAPIResponse())
		}
		return
	}

	validation := devicePIN.Validate()
	if validation != nil {
		resp.WriteResponse(w, resp.ValidationError(validation.Message).ToAPIResponse())
		return
	}

	host := helper.GetIP(r)

	appErr := h.service.SetDevicePIN(staffCode, devicePIN, host)
	if appErr != nil {
		resp.WriteResponse(w, resp.InternalError().ToAPIResponse())
		return
	}

	resp.WriteResponse(w, resp.NewAPIResponse(http.StatusOK, true, SuccessSetDevicePin, nil))
}

func (h *DeviceHandler) RemoveDevice(w http.ResponseWriter, r *http.Request) {
	staffCode, err := helper.GetStaffCodeFromContext(r)
	if err != nil {
		resp.WriteResponse(w, resp.UnauthorizedError("").ToAPIResponse())
		return
	}

	// get device pin from request body
	var request dto.RemoveDeviceRequest
	err = helper.DecodeReqBody(w, r, &request)
	if err != nil {
		if err == io.EOF {
			resp.WriteResponse(w, resp.ReqBodyEmptyError().ToAPIResponse())
		} else {
			logger.Error(err.Error())
			resp.WriteResponse(w, resp.ReqBodyInvalidError().ToAPIResponse())
		}
		return
	}

	validation := request.Validate()
	if validation != nil {
		resp.WriteResponse(w, resp.ValidationError(validation.Message).ToAPIResponse())
		return
	}

	host := helper.GetIP(r)

	appErr := h.service.RemoveDevice(request, strconv.FormatInt(staffCode, 10), host)
	if appErr != nil {
		resp.WriteResponse(w, resp.InternalError().ToAPIResponse())
		return
	}

	resp.WriteResponse(w, resp.NewAPIResponse(http.StatusOK, true, SuccessRemoveDevice, nil))
}

func NewDeviceHandler(service service.DeviceService) *DeviceHandler {
	return &DeviceHandler{service: service}
}
