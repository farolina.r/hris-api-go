package handlers

import (
	"gitlab.com/farolina.r/hris-api-go/dto"
	"gitlab.com/farolina.r/hris-api-go/helper"
	"gitlab.com/farolina.r/hris-api-go/logger"
	"gitlab.com/farolina.r/hris-api-go/resp"
	"gitlab.com/farolina.r/hris-api-go/service"
	"io"
	"net/http"
	"strconv"
)

const SuccessGetMenuMobile = "get Data Menu successfully"
const SuccessSetDefaultMenu = "You have successfully set Default Menu"

type MenuHandler struct {
	service service.MenuService
}

func (h *MenuHandler) SetDefaultMenuMobile(w http.ResponseWriter, r *http.Request) {
	staffCode, err := helper.GetStaffCodeFromContext(r)
	if err != nil {
		resp.WriteResponse(w, resp.UnauthorizedError("").ToAPIResponse())
		return
	}

	var menuData *dto.DefaultEmployeeMenuMobile
	err = helper.DecodeReqBody(w, r, &menuData)
	if err != nil {
		if err == io.EOF {
			resp.WriteResponse(w, resp.ReqBodyEmptyError().ToAPIResponse())
		} else {
			logger.Error(err.Error())
			resp.WriteResponse(w, resp.ReqBodyInvalidError().ToAPIResponse())
		}
		return
	}

	validation := menuData.Validate()
	if validation != nil {
		resp.WriteResponse(w, resp.ValidationError(validation.Message).ToAPIResponse())
		return
	}

	host := helper.GetIP(r)

	appErr := h.service.SetDefaultMenuMobile(strconv.FormatInt(staffCode, 10), host, menuData)
	if err != nil {
		resp.WriteResponse(w, appErr.ToAPIResponse())
		return
	}

	response := resp.NewAPIResponse(http.StatusOK, true, SuccessSetDefaultMenu, nil)

	resp.WriteResponse(w, response)
}

func (h *MenuHandler) GetMenuMobile(w http.ResponseWriter, r *http.Request) {
	staffCode, err := helper.GetStaffCodeFromContext(r)
	if err != nil {
		resp.WriteResponse(w, resp.UnauthorizedError("").ToAPIResponse())
		return
	}

	menu, appErr := h.service.GetMenuMobile(strconv.FormatInt(staffCode, 10))
	if err != nil {
		resp.WriteResponse(w, appErr.ToAPIResponse())
		return
	}

	response := resp.NewAPIResponse(http.StatusOK, true, SuccessGetMenuMobile, menu)

	resp.WriteResponse(w, response)
}

func NewMenuHandler(service service.MenuService) *MenuHandler {
	return &MenuHandler{service: service}
}
