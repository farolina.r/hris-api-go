package repo

import (
	"database/sql"
	"github.com/doug-martin/goqu/v9"
	"github.com/go-resty/resty/v2"
	"github.com/jmoiron/sqlx"
	"gitlab.com/farolina.r/hris-api-go/domain"
	"gitlab.com/farolina.r/hris-api-go/helper"
	"gitlab.com/farolina.r/hris-api-go/logger"
	"gitlab.com/farolina.r/hris-api-go/resp"
	"net/http"
)

const RADIUS = 750 // in meters

type MasterRepositoryDB struct {
	client       *sqlx.DB
	restyClient  *resty.Client
	employeeRepo *EmployeeRepositoryDb
}

func NewMasterRepositoryDB(client *sqlx.DB, restyClient *resty.Client, employeeRepo *EmployeeRepositoryDb) *MasterRepositoryDB {
	return &MasterRepositoryDB{
		client:       client,
		restyClient:  restyClient,
		employeeRepo: employeeRepo,
	}
}

func (m *MasterRepositoryDB) GetAttendanceLocations() ([]*domain.LocationAccess, *resp.AppError) {
	locations := make([]*domain.LocationAccess, 0)

	ss := goqu.From("MstLocationAccess").
		Where(goqu.Ex{
			"TypeAbsen": 4},
		)

	query := helper.BuildSQL(ss)
	err := m.client.Select(&locations, query)
	if err != nil {
		logger.Error(err.Error())
		if err != sql.ErrNoRows {
			return nil, &resp.AppError{
				Code:    http.StatusNotFound,
				Message: resp.NotFoundError().Message,
			}
		}
	}

	return locations, nil
}

func (m *MasterRepositoryDB) GetAttendanceRadiusLocations(staffCode int64, currCoor domain.LocationCoordinates) ([]*domain.LocationAccess, *resp.AppError) {
	locations := make([]*domain.LocationAccess, 0)

	employeeStructure, appErr := m.employeeRepo.GetEmployeeStructureByStaffCode(staffCode)
	if appErr != nil {
		return nil, appErr
	}

	query := ` SELECT mlad.ID, IDIntranet, Ip, Port, LocationName, AliasLocationName, TypeAbsen, MainLocation, Address1, Address2, Address3, Reference, Telp, Latitude, Longitude, Radius,
	   TimeZoneID, DistanceMeters
	   FROM (
	       SELECT ID, IDIntranet, Ip, Port, LocationName, AliasLocationName, TypeAbsen, MainLocation, Address1, Address2, Address3, Reference, Telp, Latitude, Longitude, Radius,
	       TimeZoneID, ((( acos( sin(( ? * pi() / 180)) * sin(( Latitude * pi() / 180)) + cos((? * pi() /180 )) *
	           cos(( Latitude * pi() / 180)) * cos(((? - Longitude) * pi()/180))) ) * 180/pi() ) * 60 * 1.1515 * 1.609344 *1000
	       ) AS DistanceMeters
	       FROM HRISDB.MstLocationAccess
	       WHERE TypeAbsen = 4 AND DeletedBy IS NULL
	   ) mlad
	   JOIN MstLocationAccessRelated mlar ON mlad.ID = mlar.LocationAccessID
	   WHERE DistanceMeters < Radius AND
	   (
	       (StaffCode IS NULL AND DivisionCode IS NULL AND DepartmentCode IS NULL AND SubDepartmentCode IS NULL AND PositionCode IS NULL) OR
	       (StaffCode LIKE '%' || ? || '%' OR DivisionCode LIKE '%' || ? || '%' OR
	       DepartmentCode LIKE '%' || ? || '%' OR SubDepartmentCode LIKE '%' || ? || '%' OR
	       PositionCode LIKE '%' || ? || '%')
	   )`

	err := m.client.Select(&locations,
		query, currCoor.Latitude,
		currCoor.Latitude, currCoor.Longitude,
		staffCode,
		employeeStructure.DivisionCode,
		employeeStructure.DepartmentCode,
		employeeStructure.SubDepartmentCode,
		employeeStructure.PositionCode,
	)
	if err != nil {
		logger.Error(err.Error())
		if err != sql.ErrNoRows {
			return nil, &resp.AppError{
				Code:    http.StatusNotFound,
				Message: resp.NotFoundError().Message,
			}
		}
	}

	return locations, nil
}
