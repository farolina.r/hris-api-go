package repo

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/doug-martin/goqu/v9"
	"github.com/go-resty/resty/v2"
	"github.com/jmoiron/sqlx"
	"gitlab.com/farolina.r/hris-api-go/domain"
	"gitlab.com/farolina.r/hris-api-go/helper"
	"gitlab.com/farolina.r/hris-api-go/logger"
	"gitlab.com/farolina.r/hris-api-go/resp"
	"net/http"
	"os"
	"strconv"
)

const MaxGetEmployeeDataCount = 25
const HRAppsURLEmployee = "http://hr-apps.int.cbn.net.id/ClientFile/employee/"

type EmployeeRepositoryDb struct {
	client      *sqlx.DB
	restyClient *resty.Client
}

func NewEmployeeRepositoryDb(db *sqlx.DB, restyClient *resty.Client) *EmployeeRepositoryDb {
	return &EmployeeRepositoryDb{
		client:      db,
		restyClient: restyClient,
	}
}

func (a *EmployeeRepositoryDb) GetEmployeeProfile(profileResponse *domain.EmployeeProfileResponse) (*domain.EmployeeProfile, *resp.ErrorResponse) {
	var profile domain.EmployeeProfile
	if profileResponse.Data != nil && len(profileResponse.Data) > 0 {
		profile = profileResponse.Data[0]
	} else {
		return nil, resp.InternalError()
	}

	return &profile, nil
}

func (a *EmployeeRepositoryDb) GetEmployeeDataByEmployeeID(employeeID string) (*domain.EmployeeData, *resp.ErrorResponse) {
	var employee domain.EmployeeData
	err := a.client.Get(&employee, "SELECT StaffCode, MaxDevice, Fullname FROM EmplyDatas WHERE NIK = ?", employeeID)
	if err != nil {
		logger.Error(err.Error())
		if err == sql.ErrNoRows {
			return nil, resp.NotFoundError()
		}
		return nil, resp.InternalError()
	}

	return &employee, nil
}

func (a *EmployeeRepositoryDb) selectEmployeePersonalData() *goqu.SelectDataset {
	return goqu.From(goqu.T("EmplyDatas").As("ed")).Join(
		goqu.T("EmplyStructures").As("ss"),
		goqu.On(goqu.Ex{
			"ed.StaffCode":        goqu.I("ss.StaffCode"),
			"ss.DefaultStructure": 1,
		}),
	).LeftJoin(
		goqu.T("MstStructures").As("msdiv"),
		goqu.On(goqu.Ex{"ss.DivisionCode": goqu.I("msdiv.id")}),
	).LeftJoin(
		goqu.T("MstStructures").As("msdept"),
		goqu.On(goqu.Ex{"ss.DepartmentCode": goqu.I("msdept.id")}),
	).LeftJoin(
		goqu.T("MstStructures").As("mssubdept"),
		goqu.On(goqu.Ex{"ss.SubDepartmentCode": goqu.I("mssubdept.id")}),
	).LeftJoin(
		goqu.T("MstStructures").As("msgroup"),
		goqu.On(goqu.Ex{"ss.GroupCode": goqu.I("msgroup.id")}),
	).Join(
		goqu.T("MstPositions").As("mp"),
		goqu.On(goqu.Ex{"ss.PositionCode": goqu.I("mp.PositionCode")}),
	).Join(
		goqu.T("AppValue").As("avs"),
		goqu.On(goqu.Ex{
			"ed.StatusEmployeeCode": goqu.I("avs.ValueCode"),
			"avs.ValueType":         "StaffStatus",
		}),
	).Join(
		goqu.T("AppValue").As("avg"),
		goqu.On(goqu.Ex{
			"avg.ValueCode": goqu.I("ed.Gender"),
			"avg.ValueType": "Gender",
		}),
	).Join(
		goqu.T("AppValue").As("avr"),
		goqu.On(goqu.Ex{
			"avr.ValueCode": goqu.I("ed.Religion"),
			"avr.ValueType": "Religion",
		}),
	).LeftJoin(
		goqu.T("MstJobFunctions").As("mjf"),
		goqu.On(goqu.Ex{"ss.JobFunctionCode": goqu.I("mjf.id")}),
	).Select(
		//&employee,
		"ed.*",
		goqu.I("msdiv.LevelName").As("Division"),
		goqu.I("msdiv.ShortLevelName").As("DivisionAlias"),
		goqu.I("msdept.LevelName").As("Department"),
		goqu.I("mssubdept.LevelName").As("SubDepartment"),
		goqu.I("msgroup.LevelName").As("Group"),
		goqu.I("avs.ValueExp").As("StaffStatusName"),
		goqu.I("avg.ValueExp").As("GenderName"),
		goqu.I("avr.ValueExp").As("ReligionName"),
		"mjf.JobFunctionName",
		"PositionName",
		"ss.PositionCode",
	)
}

func (a *EmployeeRepositoryDb) setEmployeePersonalDataImageBase(data *domain.EmployeePersonalData) {

	imageData, err := helper.GetImageBase64FromURL(HRAppsURLEmployee + strconv.FormatInt(data.StaffCode, 10) + "/" + helper.PointerStringToString(data.ProfilPict))
	if err != nil {
		logger.Error(err.Error())
		return
	}

	data.ImageBase = &imageData
}

func (a *EmployeeRepositoryDb) GetEmployeeStructureByStaffCode(staffCode int64) (*domain.EmployeeStructure, *resp.AppError) {
	var employeeStructure domain.EmployeeStructure
	ss := goqu.From("EmplyStructures").
		Where(goqu.Ex{
			"StaffCode":        staffCode,
			"DefaultStructure": 1,
			"DeletedBy":        nil,
		}).
		Select(
			"id",
			"StaffCode",
			"Company",
			"DivisionCode",
			"DepartmentCode",
			"SubDepartmentCode",
			"GroupCode",
			"PositionCode",
			"JobFunctionCode",
			"LevelCode",
		).Order(goqu.I("CreatedTime").Desc()).Limit(1)

	query := helper.BuildSQL(ss)
	err := a.client.Get(&employeeStructure, query)
	if err != nil {
		logger.Error(err.Error())
		if err == sql.ErrNoRows {
			return nil, resp.NotFoundError().ToAppError()
		}
		return nil, resp.InternalError().ToAppError()
	}

	return &employeeStructure, nil
}

func (a *EmployeeRepositoryDb) GetEmployeePersonalDataByStaffCode(staffCode int64) ([]*domain.EmployeePersonalData, *resp.ErrorResponse) {
	employees := make([]*domain.EmployeePersonalData, 0)
	ds := a.selectEmployeePersonalData()

	query := helper.BuildSQL(ds)
	query = query + ` WHERE ed.StaffCode = ?`

	err := a.client.Select(&employees, query, staffCode)
	if err != nil {
		logger.Error(err.Error())
		if err == sql.ErrNoRows {
			return employees, resp.NotFoundError()
		}
		return employees, resp.InternalError()
	}

	for _, employee := range employees {
		a.setEmployeePersonalDataImageBase(employee)
	}

	return employees, nil
}

func (a *EmployeeRepositoryDb) GetEmployeesPersonalDataBySearchKey(searchKey string) ([]*domain.EmployeePersonalData, *resp.ErrorResponse) {
	employees := make([]*domain.EmployeePersonalData, 0)
	ds := a.selectEmployeePersonalData()

	query := helper.BuildSQL(ds)
	query = query + ` WHERE Fullname LIKE '%` + searchKey + `%'
		OR NIK LIKE '%` + searchKey + `%'
		OR ChatID LIKE '%` + searchKey + `%'
		OR City LIKE '%` + searchKey + `%'
		OR CardID LIKE '%` + searchKey + `%'
		OR Address LIKE '%` + searchKey + `%'
		OR Phone LIKE '%` + searchKey + `%'
		OR BuildingCardId LIKE '%` + searchKey + `%'
		OR (DATE_FORMAT(BirthDate,'%d-%M-%Y')) LIKE '%` + searchKey + `%'
		OR Email LIKE '%` + searchKey + `%'
		OR Email2 LIKE '%` + searchKey + `%'
		OR HandPhone LIKE '%` + searchKey + `%'
		OR HandPhone2 LIKE '%` + searchKey + `%'
		OR Extension LIKE '%` + searchKey + `%'`
	query = query + ` LIMIT ` + strconv.Itoa(MaxGetEmployeeDataCount)

	err := a.client.Select(&employees, query)
	if err != nil {
		logger.Error(err.Error())
		if err == sql.ErrNoRows {
			return employees, resp.NotFoundError()
		}
		return employees, resp.InternalError()
	}

	for _, employee := range employees {
		a.setEmployeePersonalDataImageBase(employee)
	}

	return employees, nil
}

func (a *EmployeeRepositoryDb) GetEmployeeDefaultMenuByStaffCode(staffCode int64) ([]*domain.EmployeeMenuMobile, *resp.ErrorResponse) {
	employeeDefaultMenu := make([]*domain.EmployeeMenuMobile, 0)
	ds := goqu.From("EmplyMenuMobile").Where(goqu.Ex{
		"StaffCode": staffCode},
	)

	query := helper.BuildSQL(ds)

	err := a.client.Select(&employeeDefaultMenu, query)
	if err != nil {
		logger.Error(err.Error())
		if err == sql.ErrNoRows {
			return nil, resp.NotFoundError()
		}
		return nil, resp.InternalError()
	}

	return employeeDefaultMenu, nil
}

func (a *EmployeeRepositoryDb) GetEmployeeFamilyByStaffCode(staffCode int64) ([]*domain.EmployeeFamily, *resp.ErrorResponse) {
	employeeFamily := make([]*domain.EmployeeFamily, 0)
	ds := goqu.From("EmplyFamilies").Where(goqu.Ex{
		"StaffCode": staffCode},
	)

	query := helper.BuildSQL(ds)
	err := a.client.Select(&employeeFamily, query)
	if err != nil {
		logger.Error(err.Error())
		if err == sql.ErrNoRows {
			return nil, resp.NotFoundError()
		}
		return nil, resp.InternalError()
	}

	return employeeFamily, nil
}

func (a *EmployeeRepositoryDb) GetEmployeeSecondaryJobByStaffCode(staffCode int64) ([]*domain.EmployeeSecondaryJob, *resp.ErrorResponse) {
	employeeSecondaryJob := make([]*domain.EmployeeSecondaryJob, 0)
	ds := goqu.From(goqu.T("EmplyStructures").As("es")).LeftJoin(
		goqu.T("MstStructures").As("msdiv"),
		goqu.On(goqu.Ex{"msdiv.id": goqu.I("es.DivisionCode")}),
	).LeftJoin(
		goqu.T("MstStructures").As("msdept"),
		goqu.On(goqu.Ex{"msdept.id": goqu.I("es.DepartmentCode")}),
	).LeftJoin(
		goqu.T("MstStructures").As("mssubdept"),
		goqu.On(goqu.Ex{"mssubdept.id": goqu.I("es.SubDepartmentCode")}),
	).Join(
		goqu.T("MstPositions").As("mp"),
		goqu.On(goqu.Ex{"mp.PositionCode": goqu.I("es.SubDepartmentCode")}),
	).LeftJoin(
		goqu.T("MstJobFunctions").As("mjf"),
		goqu.On(goqu.Ex{"mjf.id": goqu.I("es.JobFunctionCode")}),
	).Where(goqu.Ex{
		"StaffCode":           staffCode,
		"es.DefaultStructure": 0,
	}).Select(
		//&employee,
		"es.id",
		goqu.I("msdiv.LevelName").As("Division"),
		goqu.I("msdiv.ShortLevelName").As("DivisionAlias"),
		goqu.I("msdept.LevelName").As("Department"),
		goqu.I("mssubdept.LevelName").As("SubDepartment"),
		"mjf.JobFunctionName",
		"PositionName",
		"es.PositionCode",
	)

	query := helper.BuildSQL(ds)

	err := a.client.Select(&employeeSecondaryJob, query)
	if err != nil {
		logger.Error(err.Error())
		if err == sql.ErrNoRows {
			return nil, resp.NotFoundError()
		}
		return nil, resp.InternalError()
	}

	return employeeSecondaryJob, nil
}

func (a *EmployeeRepositoryDb) GetEmployeeDevices(employeeId int64) ([]*domain.EmployeeDevice, *resp.ErrorResponse) {
	employeeDevices := make([]*domain.EmployeeDevice, 0)

	err := a.client.Select(&employeeDevices, "SELECT StaffCode, DeviceID FROM EmplyDevices WHERE StaffCode = ? AND DeletedBy IS NULL", employeeId)
	if err != nil {
		logger.Error(err.Error())
		if err == sql.ErrNoRows {
			return nil, resp.NotFoundError()
		}
		return nil, resp.InternalError()
	}

	return employeeDevices, nil
}

func (a *EmployeeRepositoryDb) GetProfileClient(accessToken string) (*domain.EmployeeProfileResponse, *resp.ErrorResponse) {
	employeeHost := os.Getenv("EMPLOYEE_APP_HOST")
	employeePort := os.Getenv("EMPLOYEE_APP_PORT")

	a.restyClient.SetContentLength(true)
	response, err := a.restyClient.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", fmt.Sprintf("Bearer %s", accessToken)).
		Get(fmt.Sprintf("%s:%s/api/v1/user/Profile", employeeHost, employeePort))

	if err != nil {
		logger.Error(err.Error())
		return nil, resp.InternalError()
	}

	if response.StatusCode() != http.StatusOK {
		logger.Error(fmt.Sprintf("error getting profile data: %v", response.String()))
		return nil, resp.ResponseStatusError(response.StatusCode(), "")
	}

	var respData domain.EmployeeProfileResponse
	err = json.Unmarshal([]byte(response.String()), &respData)
	if err != nil {
		logger.Error(err.Error())
		return nil, resp.InternalError()
	}

	return &respData, nil
}

func (a *EmployeeRepositoryDb) GetAccessTokenClient(request *domain.EmployeeAuthRequest) (*domain.EmployeeAuthResponse, *resp.ErrorResponse) {
	employeeHost := os.Getenv("EMPLOYEE_APP_HOST")
	employeePort := os.Getenv("EMPLOYEE_APP_PORT")
	employeeAuth := os.Getenv("EMPLOYEE_APP_AUTH")

	// Sets `Content-Length` header automatically
	a.restyClient.SetContentLength(true)

	// resty will set content-type to `application/x-www-form-urlencoded`
	// automatically
	response, err := a.restyClient.R().
		SetFormData(map[string]string{
			"grant_type": "password",
			"username":   request.Username,
			"password":   request.Password,
		}).
		SetHeader("Cache-Control", "no-cache").
		SetHeader("Authorization", fmt.Sprintf("Basic %s", employeeAuth)).
		Post(fmt.Sprintf("%s:%s/oauth/token", employeeHost, employeePort))

	if err != nil {
		logger.Error(err.Error())
		return nil, resp.InternalError()
	}

	if response.StatusCode() != http.StatusOK {
		logger.Error(fmt.Sprintf("error getting access token: %v", response.String()))
		return nil, resp.ResponseStatusError(response.StatusCode(), "")
	}

	var employee domain.EmployeeAuthResponse
	err = json.Unmarshal([]byte(response.String()), &employee)
	if err != nil {
		logger.Error(err.Error())
		return nil, resp.InternalError()
	}

	return &employee, nil
}
