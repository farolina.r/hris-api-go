package repo

import (
	"database/sql"
	"github.com/doug-martin/goqu/v9"
	"github.com/go-resty/resty/v2"
	"github.com/jmoiron/sqlx"
	"gitlab.com/farolina.r/hris-api-go/domain"
	"gitlab.com/farolina.r/hris-api-go/helper"
	"gitlab.com/farolina.r/hris-api-go/logger"
	"gitlab.com/farolina.r/hris-api-go/resp"
)

type MenuRepositoryDb struct {
	client      *sqlx.DB
	restyClient *resty.Client
}

func (r *MenuRepositoryDb) SetDefaultMenuMobile(staffCode, host string, request *domain.DefaultEmployeeMenuMobile) *resp.ErrorResponse {
	// delete EmplyMenuMobile where staffCode = staffCode
	ds := goqu.Delete(`EmplyMenuMobile`).Where(goqu.Ex{"StaffCode": request.StaffCode})
	query := helper.BuildSQL(ds)

	err := helper.ExecQuery(r.client, query)
	if err != nil {
		logger.Error(err.Error())
		return resp.InternalError()
	}

	//	insert DefaultEmployeeMenuMobileRequest into EmplyMenuMobile
	var is *goqu.InsertDataset
	tx, err := r.client.Beginx()
	if err != nil {
		logger.Error(err.Error())
		return resp.InternalError()
	}
	for i, menu := range request.MenuID {
		is = goqu.Insert(`EmplyMenuMobile`).Rows(goqu.Record{
			"StaffCode":   request.StaffCode,
			"MenuID":      menu,
			"SeqNo":       request.SeqNo[i],
			"Notes":       (*request.Notes)[i],
			"CreatedBy":   staffCode,
			"CreatedHost": host,
			"CreatedTime": helper.GetCurrentTimeUTC(),
		})
		query = helper.BuildSQL(is)

		_, err = tx.Exec(query)
		if err != nil {
			logger.Error(err.Error())
			_ = tx.Rollback()
			return resp.InternalError()
		}
	}

	err = tx.Commit()
	if err != nil {
		logger.Error(err.Error())
		_ = tx.Rollback()
		return resp.InternalError()
	}

	return nil
}

func (r *MenuRepositoryDb) GetMenuMobile(staffCode string) (*domain.DefaultMenuData, *resp.ErrorResponse) {
	var menus []*domain.EmployeeMenuMobile

	ds := goqu.From("EmplyMenuMobile").
		Where(goqu.Ex{"StaffCode": staffCode}).
		Select("*")

	query := helper.BuildSQL(ds)

	rows, err := r.client.Queryx(query)
	if err != nil {
		logger.Error(err.Error())
		if err == sql.ErrNoRows {
			return nil, resp.NotFoundError()
		}
		return nil, resp.InternalError()
	}
	for rows.Next() {
		var menu domain.EmployeeMenuMobile
		err = rows.StructScan(&menu)
		if err != nil {
			logger.Error(err.Error())
			return nil, resp.InternalError()
		}
		menus = append(menus, &menu)
	}

	var menuMobile []*domain.MenuMobile
	if len(menus) == 0 {
		ds = goqu.From("AppMenuMobile").
			Where(goqu.Ex{"DeletedBy": goqu.Op{"eq": nil}}).
			Select(
				"id",
				"ParrentMenuID",
				"DisplayOrder",
				goqu.C("Default").As("DefaultMenu"),
				"MenuName",
				"MenuDescription",
				"Icon",
				"HeadFlag",
				"Controller",
				"MenuPath",
				"ControllerChild",
				"MenuLevel",
			)
	} else {
		ds = goqu.From("AppMenuMobile").
			LeftJoin(goqu.T("EmplyMenuMobile").As("emm"),
				goqu.On(goqu.Ex{
					"AppMenuMobile.id": goqu.I("emm.MenuID"),
				})).
			Where(goqu.Ex{"AppMenuMobile.DeletedBy": goqu.Op{"eq": nil}}).
			Order(goqu.I("ParrentMenuID").Asc()).
			Order(goqu.I("DisplayOrder").Asc()).
			Select(
				"id",
				"ParrentMenuID",
				"DisplayOrder",
				goqu.Case().
					When(goqu.I("emm.MenuID").Eq(nil), 0).
					Else(1).
					As("DefaultMenu"),
				"MenuName",
				"MenuDescription",
				"Icon",
				"HeadFlag",
				"Controller",
				"MenuPath",
				"ControllerChild",
				"MenuLevel",
			)
	}

	query = helper.BuildSQL(ds)

	rows, err = r.client.Queryx(query)
	if err != nil {
		logger.Error(err.Error())
		if err == sql.ErrNoRows {
			return nil, resp.NotFoundError()
		}
		return nil, resp.InternalError()
	}
	for rows.Next() {
		var menu domain.MenuMobile
		err = rows.StructScan(&menu)
		if err != nil {
			logger.Error(err.Error())
			return nil, resp.InternalError()
		}
		menuMobile = append(menuMobile, &menu)
	}

	var menuTree []*domain.MenuTree

	for _, menu := range menuMobile {
		if menu.ParentMenuID == 0 {
			menuTree = append(menuTree, &domain.MenuTree{
				ID:              menu.ID,
				ParentMenuID:    menu.ParentMenuID,
				DisplayOrder:    menu.DisplayOrder,
				DefaultMenu:     menu.DefaultMenu,
				MenuName:        menu.MenuName,
				MenuDescription: menu.MenuDescription,
				Icon:            menu.Icon,
				HeadFlag:        menu.HeadFlag,
				Controller:      menu.Controller,
				MenuPath:        menu.MenuPath,
				ControllerChild: menu.ControllerChild,
				MenuLevel:       menu.MenuLevel,
			})
		}
	}

	for _, menuHead := range menuTree {
		var childMenu []*domain.MenuNode
		for _, menu := range menuMobile {
			if menu.ParentMenuID == menuHead.ID {
				childMenu = append(childMenu, &domain.MenuNode{
					ID:              menu.ID,
					ParentMenuID:    menu.ParentMenuID,
					DisplayOrder:    menu.DisplayOrder,
					DefaultMenu:     menu.DefaultMenu,
					MenuName:        menu.MenuName,
					MenuDescription: menu.MenuDescription,
					Icon:            menu.Icon,
					HeadFlag:        menu.HeadFlag,
					Controller:      menu.Controller,
					MenuPath:        menu.MenuPath,
					ControllerChild: menu.ControllerChild,
					MenuLevel:       menu.MenuLevel,
				})
			}
		}

		for _, child := range childMenu {
			var lastChildMenu []*domain.MenuMobile
			for _, menu := range menuMobile {
				if menu.ParentMenuID == child.ID {
					lastChildMenu = append(lastChildMenu, menu)
				}
			}
			child.Child = lastChildMenu
		}
		menuHead.Child = childMenu
	}

	defaultMenu := domain.DefaultMenuData{
		menuMobile,
		menuTree,
	}

	return &defaultMenu, nil
}

func NewMenuRepositoryDb(db *sqlx.DB, restyClient *resty.Client) *MenuRepositoryDb {
	return &MenuRepositoryDb{
		client:      db,
		restyClient: restyClient,
	}
}
