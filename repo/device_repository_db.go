package repo

import (
	"database/sql"
	"github.com/doug-martin/goqu/v9"
	"github.com/go-resty/resty/v2"
	"github.com/jmoiron/sqlx"
	"gitlab.com/farolina.r/hris-api-go/domain"
	"gitlab.com/farolina.r/hris-api-go/helper"
	"gitlab.com/farolina.r/hris-api-go/logger"
	"gitlab.com/farolina.r/hris-api-go/resp"
	"time"
)

type DeviceRepositoryDb struct {
	client      *sqlx.DB
	restyClient *resty.Client
}

func NewDeviceRepositoryDb(db *sqlx.DB, restyClient *resty.Client) *DeviceRepositoryDb {
	return &DeviceRepositoryDb{
		client:      db,
		restyClient: restyClient,
	}
}

func (a *DeviceRepositoryDb) RemoveDevice(employeeDevice domain.RemoveDeviceRequest, staffCode string, host string) *resp.ErrorResponse {
	ds := goqu.Update("EmplyDevices").Where(goqu.Ex{
		"StaffCode": employeeDevice.StaffCode,
		"DeviceID":  employeeDevice.DeviceID,
	}).Set(goqu.Record{
		"UpdatedBy":     staffCode,
		"UpdatedTime":   time.Now().UTC().Format("2006-01-02 15:04:05"),
		"UpdatedHost":   host,
		"DeletedBy":     staffCode,
		"ReasonDeleted": employeeDevice.ReasonDeleted,
	})

	query := helper.BuildSQL(ds)

	err := helper.ExecQuery(a.client, query)
	if err != nil {
		logger.Error(err.Error())
		return resp.InternalError()
	}

	return nil
}

func (a *DeviceRepositoryDb) SetDevicePIN(staffCode int64, devicePIN domain.DevicePIN, host string) *resp.ErrorResponse {
	appErr := a.updateDevicePINByStaffCode(staffCode, devicePIN, host)
	if appErr != nil {
		logger.Error(appErr.Message)
		return appErr
	}

	// get maxSeqNo
	ds := goqu.From("EmplyDevices").Select(
		goqu.COALESCE(goqu.MAX("SeqNo"), 0).As("SeqNo"),
	).Where(
		goqu.C("StaffCode").Eq(staffCode),
	).Limit(1)
	query := helper.BuildSQL(ds)

	var maxSeqNo int64
	err := a.client.QueryRow(query).Scan(&maxSeqNo)
	if err != nil {
		logger.Error(err.Error())
		if err != sql.ErrNoRows {
			return resp.NotFoundError()
		}
		return resp.InternalError()
	}

	// get device
	ds = goqu.From("EmplyDevices").Select(
		"StaffCode",
		"DeviceID",
	).Where(
		goqu.C("DeviceID").Eq(devicePIN.DeviceID),
	)
	query = helper.BuildSQL(ds)

	var devices []domain.EmployeeDevice
	rows, err := a.client.Queryx(query)
	if err != nil {
		logger.Error(err.Error())
		if err != sql.ErrNoRows {
			return resp.NotFoundError()
		}
		return resp.InternalError()
	}
	for rows.Next() {
		var device domain.EmployeeDevice
		err = rows.StructScan(&device)
		if err != nil {
			logger.Error(err.Error())
			return resp.InternalError()
		}
		devices = append(devices, device)
	}

	tx, err := a.client.Beginx()
	if len(devices) == 0 {
		// insert
		is := goqu.Insert("EmplyDevices").Rows(
			goqu.Record{
				"StaffCode":   staffCode,
				"DeviceID":    devicePIN.DeviceID,
				"ModelDevice": devicePIN.ModelDevice,
				"OSDevice":    devicePIN.OSDevice,
				"SeqNo":       maxSeqNo + 1,
				"Token":       devicePIN.Token,
				"CreatedBy":   staffCode,
				"CreatedHost": host,
			},
		)
		query = helper.BuildSQL(is)
		_, err = tx.Exec(query)
		if err != nil {
			logger.Error(err.Error())
			_ = tx.Rollback()
			return resp.InternalError()
		}
	} else {
		// update
		us := goqu.Update("EmplyDevices").Set(
			goqu.Record{
				"DeviceID":    devicePIN.DeviceID,
				"ModelDevice": devicePIN.ModelDevice,
				"OSDevice":    devicePIN.OSDevice,
				"Token":       devicePIN.Token,
				"UpdatedBy":   staffCode,
				"UpdatedHost": host,
				"UpdatedTime": time.Now().UTC().Format("2006-01-02 03:04:05")},
		).Where(
			goqu.C("StaffCode").Eq(staffCode),
			goqu.C("SeqNo").Eq(maxSeqNo),
		)
		query = helper.BuildSQL(us)
		_, err = tx.Exec(query)
		if err != nil {
			logger.Error(err.Error())
			err = tx.Rollback()
			if err != nil {
				return nil
			}
			return resp.InternalError()
		}
	}

	err = tx.Commit()
	if err != nil {
		logger.Error(err.Error())
		err = tx.Rollback()
		if err != nil {
			return nil
		}
		return resp.InternalError()
	}

	return nil
}

func (a *DeviceRepositoryDb) updateDevicePINByStaffCode(staffCode int64, devicePIN domain.DevicePIN, host string) *resp.ErrorResponse {
	ds := goqu.Update("EmplyDatas").Set(
		goqu.Record{
			"PIN":         devicePIN.PIN,
			"UpdatedBy":   staffCode,
			"UpdatedHost": host,
			"UpdatedTime": time.Now().UTC().Format("2006-01-02 03:04:05"),
		},
	).Where(
		goqu.Ex{"StaffCode": staffCode},
	)

	query := helper.BuildSQL(ds)
	_, err := a.client.Exec(query)
	if err != nil {
		logger.Error(err.Error())
		return resp.InternalError()
	}

	return nil
}
