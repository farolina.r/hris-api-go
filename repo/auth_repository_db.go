package repo

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/doug-martin/goqu/v9"
	"github.com/go-resty/resty/v2"
	"github.com/jmoiron/sqlx"
	"gitlab.com/farolina.r/hris-api-go/domain"
	"gitlab.com/farolina.r/hris-api-go/helper"
	"gitlab.com/farolina.r/hris-api-go/logger"
	"gitlab.com/farolina.r/hris-api-go/resp"
	"net/http"
	"time"
)

const ErrorMsgDeviceFull = "Device is full, cannot register device again."
const ErrorMsgEmployeeNotFound = "Employee Data Don't Exist"
const ErrorMsgAppKeyNotValid = "App key not Valid"
const SuccessMsgLoggedOut = "You have successfully Logged Out"

type AuthRepositoryDb struct {
	client       *sqlx.DB
	restyClient  *resty.Client
	employeeRepo *EmployeeRepositoryDb
}

type RemoteAuthRepository struct {
}

func (a AuthRepositoryDb) Logout(session domain.AppTokenSession) (*resp.APIResponse, *resp.ErrorResponse) {
	//	 update AppTokenSession
	ds := goqu.Update("AppTokenSession").Set(goqu.Record{
		"ExpiredAt":   helper.GetCurrentTimeUTC(),
		"UpdatedBy":   session.StaffCode,
		"UpdatedHost": session.CreatedHost,
		"UpdatedTime": helper.GetCurrentTimeUTC(),
	}).Where(goqu.Ex{
		"StaffCode": session.StaffCode,
		"api_token": session.BlackpineToken,
	})

	query := helper.BuildSQL(ds)
	err := helper.ExecQuery(a.client, query)
	if err != nil {
		return nil, resp.InternalError()
	}

	// delete device
	dds := goqu.Update("EmplyDevices").Set(goqu.Record{
		"ReasonDeleted": "LogOut",
		"DeletedBy":     session.StaffCode,
		"UpdatedBy":     session.StaffCode,
		"UpdatedHost":   session.CreatedHost,
		"UpdatedTime":   helper.GetCurrentTimeUTC(),
	}).Where(goqu.Ex{
		"StaffCode": session.StaffCode,
		"Token":     session.BlackpineToken,
	})

	query = helper.BuildSQL(dds)

	err = helper.ExecQuery(a.client, query)
	if err != nil {
		return nil, resp.InternalError()
	}

	return resp.NewAPIResponse(
		http.StatusOK,
		true,
		SuccessMsgLoggedOut,
		nil,
	), nil
}

func (a AuthRepositoryDb) Login(login domain.Login, host string) (*domain.LoginResponse, *resp.AppError) {
	appKeyQuery := `SELECT * FROM AppUsers WHERE username = 'dont-panic' and remember_token = ?`

	var appKey domain.AppUser
	err := a.client.Get(&appKey, appKeyQuery, login.Token)
	if err != nil {
		logger.Error(err.Error())
		if err == sql.ErrNoRows {
			return nil, &resp.AppError{
				Code:    http.StatusNotFound,
				Message: resp.NotFoundError().Message,
			}
		}
		return nil, &resp.AppError{
			Code:    http.StatusInternalServerError,
			Message: resp.InternalError().Message,
		}
	}

	var employeeData *domain.EmployeeData
	var response *domain.LoginResponse
	var token string

	if (domain.AppUser{}) != appKey {
		serv := a.employeeRepo
		empAuthResp, err := serv.GetAccessTokenClient(&domain.EmployeeAuthRequest{
			Username: login.Username,
			Password: login.Password,
		})
		if err != nil {
			return nil, &resp.AppError{
				Code:    err.Code,
				Message: err.Message,
			}
		}

		profileResponse, err := serv.GetProfileClient(empAuthResp.AccessToken)
		if err != nil {
			return nil, &resp.AppError{
				Code:    err.Code,
				Message: err.Message,
			}
		}

		profile, err := serv.GetEmployeeProfile(profileResponse)
		if err != nil {
			return nil, &resp.AppError{
				Code:    err.Code,
				Message: err.Message,
			}
		}

		employeeData, err = serv.GetEmployeeDataByEmployeeID(profile.EmplId)
		if err != nil {
			return nil, &resp.AppError{
				Code:    err.Code,
				Message: err.Message,
			}
		}

		if employeeData != nil {
			employeeDevices, err := serv.GetEmployeeDevices(employeeData.StaffCode)
			if err != nil {
				return nil, &resp.AppError{
					Code:    err.Code,
					Message: err.Message,
				}
			}

			if len(employeeDevices) < employeeData.MaxDevice {
				var expired time.Time
				expiresIn := time.Unix(empAuthResp.ExpiresIn, 0)
				expired = expiresIn.UTC()
				if expiresIn.Before(time.Now()) {
					// add 24 hours
					expired = time.Now().Add(domain.ExpirationTime).UTC()
				}

				var loginUserData domain.LoginUserData
				d, err := json.Marshal(employeeData)
				if err != nil {
					return nil, &resp.AppError{
						Code:    http.StatusInternalServerError,
						Message: resp.InternalError().Message,
					}
				}
				err = json.Unmarshal(d, &loginUserData)
				if err != nil {
					logger.Error(err.Error())
					return nil, &resp.AppError{
						Code:    http.StatusInternalServerError,
						Message: resp.InternalError().Message,
					}
				}

				// set expired at for login user data
				loginUserData.ExpiresIn = helper.AddTimeUTC(time.Now(), domain.ExpirationTime)

				token, err = loginUserData.GenerateToken()
				if err != nil {
					return nil, &resp.AppError{
						Code:    http.StatusInternalServerError,
						Message: resp.InternalError().Message,
					}
				}

				refreshToken, _ := loginUserData.GenerateRefreshToken()

				appErr := a.InsertAppTokenSession(domain.AppTokenSession{
					StaffCode:      employeeData.StaffCode,
					UserCode:       profile.UserCode,
					BlackpineToken: empAuthResp.AccessToken,
					JWTToken:       token,
					BranchCode:     &profile.BranchCode,
					ExpiredAt:      expired,
					CreatedBy:      profile.UserCode,
					CreatedHost:    host,
				})
				if appErr != nil {
					return nil, &resp.AppError{
						Code:    appErr.Code,
						Message: appErr.Message,
					}
				}

				response = &domain.LoginResponse{
					Name:           employeeData.FullName,
					StatusCode:     http.StatusText(http.StatusOK),
					BlackpineToken: empAuthResp.AccessToken,
					JWTToken:       token,
					RefreshToken:   refreshToken,
					TokenType:      empAuthResp.TokenType,
					ExpiresIn:      expired,
				}

			} else {
				return nil, &resp.AppError{
					Code:    http.StatusBadRequest,
					Message: ErrorMsgDeviceFull,
				}
			}
		} else {
			return nil, &resp.AppError{
				Code:    http.StatusBadRequest,
				Message: ErrorMsgEmployeeNotFound,
			}
		}
	} else {
		return nil, &resp.AppError{
			Code:    http.StatusBadRequest,
			Message: ErrorMsgAppKeyNotValid,
		}
	}

	return response, nil
}

func (a AuthRepositoryDb) InsertAppTokenSession(appTokenSession domain.AppTokenSession) *resp.ErrorResponse {
	tx, err := a.client.Beginx()
	query := `INSERT INTO AppTokenSession (StaffCode, UserCode, api_token, jwt_token, BranchCode, ExpiredAt, CreatedBy, CreatedHost) VALUES (?, ?, ?, ?, ?, ?, ?, ?)`
	_, err = tx.Exec(query, appTokenSession.StaffCode, appTokenSession.UserCode, appTokenSession.BlackpineToken, appTokenSession.JWTToken, appTokenSession.BranchCode, appTokenSession.ExpiredAt, appTokenSession.CreatedBy, appTokenSession.CreatedHost)

	if err != nil {
		logger.Error(err.Error())
		rollBackErr := tx.Rollback()
		if rollBackErr != nil {
			logger.Error(rollBackErr.Error())
			return resp.InternalError()
		}
		if err == sql.ErrNoRows {
			return resp.NotFoundError()
		}
		return resp.InternalError()
	}

	err = tx.Commit()
	if err != nil {
		logger.Error(err.Error())
		rollBackErr := tx.Rollback()
		if rollBackErr != nil {
			logger.Error(rollBackErr.Error())
			return resp.InternalError()
		}
		if err == sql.ErrNoRows {
			return resp.NotFoundError()
		}
		return resp.InternalError()
	}

	return nil
}

func (a AuthRepositoryDb) updateJWTAppTokenSession(appTokenSession domain.AppTokenSession, host string) *resp.ErrorResponse {
	ds := goqu.From("AppTokenSession").
		Update().Set(goqu.Record{
		"UpdatedTime": helper.GetCurrentTimeUTC(),
		"UpdatedBy":   appTokenSession.StaffCode,
		"UpdatedHost": host,
		"jwt_token":   appTokenSession.JWTToken,
		"ExpiredAt":   helper.ConvertTimeUTCToString(appTokenSession.ExpiredAt),
	}).Where(goqu.Ex{
		"StaffCode": appTokenSession.StaffCode,
		"api_token": appTokenSession.BlackpineToken,
		"DeletedBy": nil,
	})
	query := helper.BuildSQL(ds)

	err := helper.ExecQuery(a.client, query)
	if err != nil {
		logger.Error(err.Error())
		return resp.InternalError()
	}

	return nil
}

func (a AuthRepositoryDb) RefreshToken(accessToken string, refreshToken string, host string) (*domain.RefreshAuth, *resp.ErrorResponse) {
	var response *domain.RefreshAuth
	tokenInvalid := false

	// verify jwt refreshToken
	claims, appErr := domain.AuthClaims{Token: refreshToken}.Verify()
	if appErr != nil {
		if appErr.Code == http.StatusUnauthorized {
			tokenInvalid = true
		} else {
			return nil, appErr
		}
	}

	// delete previous session
	if tokenInvalid {
		_, appErr = a.RemoveUserSessionByJWTToken(accessToken, host)
		if appErr != nil {
			return nil, appErr
		}
		return nil, resp.ForbiddenError(resp.ErrRefreshTokenInvalidRetryLogin.Error())
	}

	// get previous session
	session, appErr := a.GetUserSessionByStaffCodeAndJWTToken(claims.StaffCode, accessToken)
	if appErr != nil {
		if appErr.Code == http.StatusNotFound {
			return nil, resp.OtherError(resp.ErrTokenInvalid)
		}
		return nil, appErr
	}

	loginUserData := domain.LoginUserData{
		StaffCode: claims.StaffCode,
		MaxDevice: claims.MaxDevice,
	}

	newAccessToken, err := loginUserData.GenerateToken()
	if err != nil {
		return nil, resp.InternalError()
	}

	newRefreshToken, _ := loginUserData.GenerateRefreshToken()

	// update session, because api_token is primary key
	appErr = a.updateJWTAppTokenSession(domain.AppTokenSession{
		StaffCode:      session.StaffCode,
		BlackpineToken: session.BlackpineToken,
		JWTToken:       newAccessToken,
		ExpiredAt:      helper.AddTimeUTC(time.Now(), domain.ExpirationTime),
	}, host)
	if appErr != nil {
		return nil, appErr
	}

	response = &domain.RefreshAuth{
		AccessToken:  newAccessToken,
		RefreshToken: newRefreshToken,
	}

	return response, nil
}

func (a AuthRepositoryDb) GetUserSessionByStaffCodeAndJWTToken(staffCode int64, token string) (*domain.AppTokenSession, *resp.ErrorResponse) {
	var appTokenSession domain.AppTokenSession
	ds := goqu.From("AppTokenSession").
		Order(goqu.I("ExpiredAt").Desc()).
		Where(goqu.Ex{
			"StaffCode": staffCode,
			"jwt_token": token,
			//"ExpiredAt": goqu.Op{"gt": helper.GetCurrentTimeUTC()},
			"DeletedBy": nil,
		}).Select(
		"StaffCode", "api_token", "jwt_token", "BranchCode", "ExpiredAt", "CreatedBy", "CreatedHost",
	).Limit(1)
	query := helper.BuildSQL(ds)
	err := a.client.Get(&appTokenSession, query)
	if err != nil {
		logger.Error(fmt.Sprintf("Token: %s, error: %s", token, err.Error()))
		if err == sql.ErrNoRows {
			return nil, resp.NotFoundError()
		}
		return nil, resp.InternalError()
	}

	return &appTokenSession, nil
}

func (a AuthRepositoryDb) RemoveUserSessionByJWTToken(token string, host string) (*resp.APIResponse, *resp.ErrorResponse) {
	var response *resp.APIResponse
	ds := goqu.From("AppTokenSession").
		Update().Set(goqu.Record{
		"UpdatedTime": helper.GetCurrentTimeUTC(),
		"UpdatedHost": host,
		"DeletedBy":   domain.UserSystem,
	}).Where(goqu.Ex{
		"jwt_token": token,
		//"ExpiredAt": goqu.Op{"lt": helper.GetCurrentTimeUTC()},
		"DeletedBy": nil,
	})
	query := helper.BuildSQL(ds)
	err := helper.ExecQuery(a.client, query)
	if err != nil {
		logger.Error(err.Error())
		return nil, resp.InternalError()
	}

	response = resp.NewAPIResponse(http.StatusOK, true, "", nil)

	return response, nil
}

func (a AuthRepositoryDb) GetUserByToken(token string) (*domain.AppTokenSession, *resp.ErrorResponse) {
	var appTokenSession domain.AppTokenSession
	ds := goqu.From("AppTokenSession").Where(goqu.Ex{
		"api_token": token,
		//"ExpiredAt": goqu.Op{"gt": helper.GetCurrentTimeUTC()},
		"DeletedBy": nil,
	}).Select(
		"StaffCode", "api_token", "ExpiredAt", "CreatedBy", "CreatedHost",
	)
	query := helper.BuildSQL(ds)
	err := a.client.Get(&appTokenSession, query)
	if err != nil {
		logger.Error(err.Error())
		if err == sql.ErrNoRows {
			return nil, resp.NotFoundError()
		}
		return nil, resp.InternalError()
	}

	return &appTokenSession, nil
}

func NewAuthRepository(client *sqlx.DB, restyClient *resty.Client, employeeRepo *EmployeeRepositoryDb) domain.AuthRepository {
	return &AuthRepositoryDb{
		client:       client,
		restyClient:  restyClient,
		employeeRepo: employeeRepo,
	}
}
