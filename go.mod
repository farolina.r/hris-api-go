module gitlab.com/farolina.r/hris-api-go

go 1.17

require (
	github.com/doug-martin/goqu/v9 v9.18.0
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/go-resty/resty/v2 v2.7.0
	github.com/go-sql-driver/mysql v1.6.0
	github.com/golang-jwt/jwt/v4 v4.4.1
	github.com/gorilla/mux v1.8.0
	github.com/jmoiron/sqlx v1.3.4
	github.com/joho/godotenv v1.4.0
	go.uber.org/zap v1.21.0
)

require (
	github.com/lib/pq v1.10.5 // indirect
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.19.0 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	golang.org/x/net v0.0.0-20220325170049-de3da57026de // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
