package app

import (
	"fmt"
	"github.com/go-redis/redis"
	"github.com/go-resty/resty/v2"
	"github.com/jmoiron/sqlx"
	"gitlab.com/farolina.r/hris-api-go/handlers"
	"gitlab.com/farolina.r/hris-api-go/helper"
	"gitlab.com/farolina.r/hris-api-go/logger"
	"gitlab.com/farolina.r/hris-api-go/middleware"
	"gitlab.com/farolina.r/hris-api-go/repo"
	"gitlab.com/farolina.r/hris-api-go/service"
	"gitlab.com/farolina.r/hris-api-go/store"
	"log"
	"net/http"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

func sanityCheck() {
	if os.Getenv("DB_USERNAME") == "" || os.Getenv("DB_PASSWORD") == "" || os.Getenv("DB_HOST") == "" || os.Getenv("DB_PORT") == "" || os.Getenv("DB_DATABASE") == "" {
		log.Fatal("env is not set")
	}
}

func Start() {
	sanityCheck()
	router := mux.NewRouter()
	dbClient := connectToDatabase()
	restyClient := connectToResty()
	redisClient := connectToRedis()

	// handlers
	employeeRepo := repo.NewEmployeeRepositoryDb(dbClient, restyClient)
	employeeHandler := handlers.NewEmployeeHandler(service.NewEmployeeService(employeeRepo))

	deviceHandler := handlers.NewDeviceHandler(service.NewDeviceService(repo.NewDeviceRepositoryDb(dbClient, restyClient)))

	menuHandler := handlers.NewMenuHandler(service.NewMenuService(repo.NewMenuRepositoryDb(dbClient, restyClient)))

	authRepo := repo.NewAuthRepository(dbClient, restyClient, employeeRepo)
	authStore := store.NewAuthStore(redisClient, authRepo)
	authHandler := handlers.NewAuthHandler(service.NewAuthService(authStore, authRepo))

	masterHandler := handlers.NewMasterHandler(service.NewMasterService(repo.NewMasterRepositoryDB(dbClient, restyClient, employeeRepo)))

	// routes
	router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte("Hello World"))
		if err != nil {
			return
		}
	})

	// un auth routes
	unauthenticatedRoute := router.PathPrefix("/api").Subrouter()
	unauthenticatedRoute.HandleFunc("/login", authHandler.Login).Methods(http.MethodPost)
	unauthenticatedRoute.HandleFunc("/refreshToken", authHandler.RefreshToken).Methods(http.MethodPost)
	unauthenticatedRoute.HandleFunc("/locations", masterHandler.GetAttendanceLocations).Methods(http.MethodPost)

	// auth routes
	authenticatedRoute := router.PathPrefix("/api").Subrouter()

	authenticatedRoute.HandleFunc("/logout", authHandler.Logout).Methods(http.MethodPost)

	authenticatedRoute.HandleFunc("/personalInfo", employeeHandler.GetEmployeePersonalInfo).Methods(http.MethodPost)

	authenticatedRoute.HandleFunc("/setDevicePIN", deviceHandler.SetDevicePIN).Methods(http.MethodPost)
	authenticatedRoute.HandleFunc("/removeDevice", deviceHandler.RemoveDevice).Methods(http.MethodPost)

	authenticatedRoute.HandleFunc("/getMenuMobile", menuHandler.GetMenuMobile).Methods(http.MethodPost)
	authenticatedRoute.HandleFunc("/setDefaultMenuMobile", menuHandler.SetDefaultMenuMobile).Methods(http.MethodPost)

	authenticatedRoute.HandleFunc("/getLocationPresence", masterHandler.GetAttendanceRadiusLocations).Methods(http.MethodPost)

	am := middleware.NewAuthMiddleware(authRepo)
	authenticatedRoute.Use(am.AuthorizationHandler())

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", helper.Getenv("APP_PORT", "8000")), router))
}

func connectToDatabase() *sqlx.DB {
	dbUsername := os.Getenv("DB_USERNAME")
	dbPassword := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_DATABASE")
	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")

	// open db connection
	dataSourceName := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=true", dbUsername, dbPassword, dbHost, dbPort, dbName)
	logger.Info("Connecting to database: " + dataSourceName)
	client, err := sqlx.Open("mysql", dataSourceName)
	if err != nil {
		panic(err)
	}

	client.SetConnMaxLifetime(time.Minute * 3)
	client.SetMaxOpenConns(10)
	client.SetMaxIdleConns(10)

	return client
}

func connectToResty() *resty.Client {
	restyClient := resty.New()
	restyClient.SetTimeout(time.Second * 20)
	restyClient.SetRetryCount(3)
	restyClient.SetRetryWaitTime(time.Second * 3)

	logger.Info("Connecting to resty")

	return restyClient
}

func connectToRedis() *redis.Client {
	client := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%s", os.Getenv("REDIS_HOST"), os.Getenv("REDIS_PORT")),
		Password: os.Getenv("REDIS_PASSWORD"),
		DB:       0,
	})
	if err := client.Ping().Err(); err != nil {
		logger.Error("redis connection error: " + err.Error())
		return nil
	}
	logger.Info("Connecting to redis: " + client.Options().Addr)

	return client
}
