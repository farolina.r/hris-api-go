package domain

import (
	"gitlab.com/farolina.r/hris-api-go/resp"
)

type DeviceRepository interface {
	SetDevicePIN(int64, DevicePIN, string) *resp.ErrorResponse
	RemoveDevice(RemoveDeviceRequest, string, string) *resp.ErrorResponse
}

type DevicePIN struct {
	DeviceID    *string `db:"DeviceID"`
	PIN         *string `db:"PIN"`
	Token       *string `db:"Token"`
	ModelDevice *string `db:"ModelDevice"`
	OSDevice    *string `db:"OSDevice"`
}

type EmployeeDevice struct {
	StaffCode string  `db:"StaffCode"`
	DeviceID  *string `db:"DeviceID"`
}

type RemoveDeviceRequest struct {
	StaffCode     int64   `json:"StaffCode"`
	DeviceID      string  `json:"DeviceID"`
	ReasonDeleted *string `json:"ReasonDeleted"`
}
