package domain

import (
	"fmt"
	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/farolina.r/hris-api-go/logger"
	"gitlab.com/farolina.r/hris-api-go/resp"
	"os"
	"time"
)

type AuthClaims struct {
	Token string
}

type AuthTokenClaims struct {
	StaffCode int64 `json:"StaffCode"`
	MaxDevice int   `json:"MaxDevice"`
	jwt.RegisteredClaims
}

func (c *AuthTokenClaims) Validate() error {
	// TODO: add more verification if needed

	expiresAt := c.RegisteredClaims.ExpiresAt.Time
	now := time.Now()

	if now.After(expiresAt) {
		return resp.ErrTokenExpired
	}

	return nil
}

func (a AuthClaims) Verify() (*AuthTokenClaims, *resp.ErrorResponse) {
	jwtToken, err := jwt.ParseWithClaims(a.Token, &AuthTokenClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(os.Getenv("AUTH_SECRET")), nil
	})
	if err != nil {
		logger.Error(fmt.Sprintf("Token can't be parsed: %v, error: %v", jwtToken, err))
		return nil, resp.UnauthorizedError(resp.ErrTokenInvalid.Error())
	}

	if claims, ok := jwtToken.Claims.(*AuthTokenClaims); ok && jwtToken.Valid {
		err = claims.Validate()
		if err != nil {
			return nil, resp.OtherError(err)
		}
		return claims, nil
	} else {
		logger.Error(fmt.Sprintf("Token is invalid: %v", jwtToken))
		return nil, resp.UnauthorizedError(resp.ErrTokenInvalid.Error())
	}
}
