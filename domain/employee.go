package domain

import (
	"encoding/json"
	"gitlab.com/farolina.r/hris-api-go/dto"
	"gitlab.com/farolina.r/hris-api-go/logger"
	"gitlab.com/farolina.r/hris-api-go/resp"
	"time"
)

type EmployeeRepository interface {
	GetAccessTokenClient(request *EmployeeAuthRequest) (*EmployeeAuthResponse, *resp.ErrorResponse)
	GetProfileClient(accessToken string) (*EmployeeProfileResponse, *resp.ErrorResponse)
	GetEmployeeDevices(employeeId int64) ([]*EmployeeDevice, *resp.ErrorResponse)
	GetEmployeeProfile(*EmployeeProfileResponse) (*EmployeeProfile, *resp.ErrorResponse)
	GetEmployeeDataByEmployeeID(employeeID string) (*EmployeeData, *resp.ErrorResponse)
	GetEmployeePersonalDataByStaffCode(int64) ([]*EmployeePersonalData, *resp.ErrorResponse)
	GetEmployeesPersonalDataBySearchKey(searchKey string) ([]*EmployeePersonalData, *resp.ErrorResponse)
	GetEmployeeDefaultMenuByStaffCode(int64) ([]*EmployeeMenuMobile, *resp.ErrorResponse)
	GetEmployeeFamilyByStaffCode(int64) ([]*EmployeeFamily, *resp.ErrorResponse)
	GetEmployeeSecondaryJobByStaffCode(int64) ([]*EmployeeSecondaryJob, *resp.ErrorResponse)
	GetEmployeeStructureByStaffCode(int64) (*EmployeeStructure, *resp.AppError)
}

type EmployeeAuthRequest struct {
	GrantType string `json:"grant_type"`
	Username  string `json:"username"`
	Password  string `json:"password"`
}

type EmployeeAuthResponse struct {
	AccessToken string `json:"access_token"`
	TokenType   string `json:"token_type"`
	ExpiresIn   int64  `json:"expires_in"`
}

type EmployeeLoginData struct {
	StaffCode int64 `json:"staff_code"`
}

type EmployeeProfile struct {
	UserCode    string `json:"UserCode"`
	FullName    string `json:"FullName"`
	DisplayName string `json:"DisplayName"`
	Email       string `json:"Email"`
	EmplId      string `json:"Empl_id"`
	BranchCode  string `json:"BranchCode"`
}

type EmployeeProfileResponse struct {
	Response int               `json:"response"`
	Message  string            `json:"message"`
	Data     []EmployeeProfile `json:"data"`
}

type EmployeeData struct {
	StaffCode int64  `db:"StaffCode"`
	MaxDevice int    `db:"MaxDevice"`
	FullName  string `db:"Fullname"`
}

type EmployeePersonalData struct {
	StaffCode          int64      `db:"StaffCode"`
	PIN                *string    `db:"PIN"`
	MaxDevice          int        `db:"MaxDevice"`
	NIK                string     `db:"NIK"`
	CardID             *string    `db:"CardID"`
	BuildingCardId     *string    `db:"BuildingCardId"`
	AbsenType          *string    `db:"AbsenType"`
	IdNumber           string     `db:"IdNumber"`
	NoKK               *string    `db:"NoKK"`
	FullName           string     `db:"Fullname"`
	ShortName          *string    `db:"ShortName"`
	SignInDate         time.Time  `db:"SignInDate"`
	ResignDate         *time.Time `db:"ResignDate"`
	FailedJoinDate     *time.Time `db:"FailedJoinDate"`
	StatusEmployeeCode int32      `db:"StatusEmployeeCode"`
	BirthPlace         *string    `db:"BirthPlace"`
	BirthDate          *time.Time `db:"BirthDate"`
	Phone              *string    `db:"Phone"`
	HandPhone          *string    `db:"HandPhone"`
	Handphone2         *string    `db:"Handphone2"`
	Email              *string    `db:"Email"`
	Email2             *string    `db:"Email2"`
	ChatID             *string    `db:"ChatID"`
	Address            *string    `db:"Address"`
	City               *string    `db:"City"`
	ZipCode            *string    `db:"ZipCode"`
	AddressDomisili    *string    `db:"AddressDomisili"`
	CityDomisili       *string    `db:"CityDomisili"`
	ZipCodeDomisili    *string    `db:"ZipCodeDomisili"`
	Extension          *string    `db:"Extension"`
	Gender             string     `db:"Gender"`
	Religion           *string    `db:"Religion"`
	PassportNo         *string    `db:"PassportNo"`
	BloodType          *string    `db:"BloodType"`
	BankAccountNo      *string    `db:"BankAccountNo"`
	NPWPNo             *string    `db:"NPWPNo"`
	NPWPDate           *time.Time `db:"NPWPDate"`
	LastNIK            *string    `db:"LastNIK"`
	CustomerIDCBN      *string    `db:"CustomerIDCBN"`
	ProfilPict         *string    `db:"ProfilPict"`
	IdCardPict         *string    `db:"IdCardPict"`
	FamilyCardPict     *string    `db:"FamilyCardPict"`
	VaccineCardPict1   *string    `db:"VaccineCardPict1"`
	VaccineCardPict2   *string    `db:"VaccineCardPict2"`
	DeviceID1          *string    `db:"DeviceID1"`
	DeviceID2          *string    `db:"DeviceID2"`
	Notes              *string    `db:"Notes"`
	CreatedBy          string     `db:"CreatedBy"`
	CreatedTime        time.Time  `db:"CreatedTime"`
	CreatedHost        string     `db:"CreatedHost"`
	UpdatedBy          *string    `db:"UpdatedBy"`
	UpdatedTime        *time.Time `db:"UpdatedTime"`
	UpdatedHost        *string    `db:"UpdatedHost"`
	DeletedBy          *string    `db:"DeletedBy"`
	ReasonDeleted      *string    `db:"ReasonDeleted"`
	Division           *string    `db:"Division"`
	DivisionAlias      *string    `db:"DivisionAlias"`
	Department         *string    `db:"Department"`
	SubDepartment      *string    `db:"SubDepartment"`
	Group              *string    `db:"Group"`
	StaffStatusName    string     `db:"StaffStatusName"`
	GenderName         string     `db:"GenderName"`
	ReligionName       string     `db:"ReligionName"`
	JobFunctionName    *string    `db:"JobFunctionName"`
	PositionName       *string    `db:"PositionName"`
	PositionCode       string     `db:"PositionCode"`
	ImageBase          *string    `db:"ImageBase"`
}

type EmployeeSchedule struct {
}

type EmployeeFamily struct {
	ID              int64      `db:"id"`
	StaffCode       int64      `db:"StaffCode"`
	SeqNo           int        `db:"SeqNo"`
	TypeFamily      int32      `db:"TypeFamily"`
	Fullname        string     `db:"Fullname"`
	IdNumber        *string    `db:"IdNumber"`
	Gender          string     `db:"Gender"`
	BloodType       string     `db:"BloodType"`
	BirthPlace      string     `db:"BirthPlace"`
	BirthDate       time.Time  `db:"BirthDate"`
	HandPhone       *string    `db:"HandPhone"`
	Claim           *int32     `db:"Claim"`
	CertificateNo   *string    `db:"CertificateNo"`
	CertificateDate *time.Time `db:"CertificateDate"`
	Occupation      *string    `db:"Occupation"`
	EducationCode   *int32     `db:"EducationCode"`
	MotherName      *string    `db:"MotherName"`
	Citizenship     string     `db:"Citizenship"`
	NPWPNo          *string    `db:"NPWPNo"`
	CreatedBy       string     `db:"CreatedBy"`
	CreatedTime     time.Time  `db:"CreatedTime"`
	CreatedHost     string     `db:"CreatedHost"`
	UpdatedBy       *string    `db:"UpdatedBy"`
	UpdatedTime     *time.Time `db:"UpdatedTime"`
	UpdatedHost     *string    `db:"UpdatedHost"`
	DeletedBy       *string    `db:"DeletedBy"`
	ReasonDeleted   *string    `db:"ReasonDeleted"`
}

type EmployeeSecondaryJob struct {
	ID              int64  `db:"id"`
	Division        string `db:"Division"`
	DivisionAlias   string `db:"DivisionAlias"`
	Department      string `db:"Department"`
	SubDepartment   string `db:"SubDepartment"`
	JobFunctionName string `db:"JobFunctionName"`
	PositionName    string `db:"PositionName"`
	PositionCode    string `db:"PositionCode"`
}

type EmployeeStructure struct {
	ID                int64   `db:"id"`
	StaffCode         int64   `db:"StaffCode"`
	Company           int32   `db:"Company"`
	DivisionCode      *int32  `db:"DivisionCode"`
	DepartmentCode    *int32  `db:"DepartmentCode"`
	SubDepartmentCode *int32  `db:"SubDepartmentCode"`
	GroupCode         *int32  `db:"GroupCode"`
	LevelCode         *string `db:"LevelCode"`
	JobFunctionCode   *int32  `db:"JobFunctionCode"`
	PositionCode      string  `db:"PositionCode"`
}

func (e EmployeePersonalData) ToDTO() *dto.EmployeePersonalData {
	employeePersonalData := dto.EmployeePersonalData{}
	d, err := json.Marshal(e)
	if err != nil {
		logger.Error(err.Error())
	}

	err = json.Unmarshal(d, &employeePersonalData)
	if err != nil {
		logger.Error(err.Error())
	}

	return &employeePersonalData
}

func (e EmployeeFamily) ToDTO() *dto.EmployeeFamily {
	employeeFamily := dto.EmployeeFamily{}
	d, err := json.Marshal(e)
	if err != nil {
		logger.Error(err.Error())
	}

	err = json.Unmarshal(d, &employeeFamily)
	if err != nil {
		logger.Error(err.Error())
	}

	return &employeeFamily
}

func (e EmployeeSecondaryJob) ToDTO() *dto.EmployeeSecondaryJob {
	employeeSecondaryJob := &dto.EmployeeSecondaryJob{}
	d, err := json.Marshal(e)
	if err != nil {
		logger.Error(err.Error())
	}

	err = json.Unmarshal(d, &employeeSecondaryJob)
	if err != nil {
		logger.Error(err.Error())
	}

	return employeeSecondaryJob
}
