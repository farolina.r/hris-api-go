package domain

import (
	"encoding/json"
	"gitlab.com/farolina.r/hris-api-go/dto"
	"gitlab.com/farolina.r/hris-api-go/logger"
	"gitlab.com/farolina.r/hris-api-go/resp"
	"time"
)

type MasterRepository interface {
	GetAttendanceLocations() ([]*LocationAccess, *resp.AppError)
	GetAttendanceRadiusLocations(staffCode int64, coordinates LocationCoordinates) ([]*LocationAccess, *resp.AppError)
}

type LocationCoordinates struct {
	Latitude  *float64 `json:"latitude"`
	Longitude *float64 `json:"longitude"`
}

type LocationAccess struct {
	ID                int32      `db:"ID"`
	IDIntranet        *string    `db:"IDIntranet"`
	Ip                *string    `db:"Ip"`
	Port              *string    `db:"Port"`
	LocationName      string     `db:"LocationName"`
	AliasLocationName *string    `db:"AliasLocationName"`
	TypeAbsen         int8       `db:"TypeAbsen"`
	MainLocation      *string    `db:"MainLocation"`
	Address1          *string    `db:"Address1"`
	Address2          *string    `db:"Address2"`
	Address3          *string    `db:"Address3"`
	Reference         *string    `db:"Reference"`
	Telp              *string    `db:"Telp"`
	Latitude          *float64   `db:"Latitude"`
	Longitude         *float64   `db:"Longitude"`
	Radius            *int       `db:"Radius"`
	TimeZoneID        *string    `db:"TimeZoneID"`
	Notes             *string    `db:"Notes,omitempty"`
	CreatedBy         string     `db:"CreatedBy,omitempty"`
	CreatedHost       string     `db:"CreatedHost,omitempty"`
	CreatedTime       time.Time  `db:"CreatedTime,omitempty"`
	UpdatedBy         *string    `db:"UpdatedBy,omitempty"`
	UpdatedHost       *string    `db:"UpdatedHost,omitempty"`
	UpdatedTime       *time.Time `db:"UpdatedTime,omitempty"`
	ReasonDeleted     *string    `db:"ReasonDeleted,omitempty"`
	DeletedBy         *string    `db:"DeletedBy,omitempty"`
	DistanceMeters    *float64   `db:"DistanceMeters,omitempty"`
}

func (l LocationAccess) ToDTO() *dto.LocationAccess {
	return &dto.LocationAccess{
		ID:                l.ID,
		IDIntranet:        l.IDIntranet,
		Ip:                l.Ip,
		Port:              l.Port,
		LocationName:      l.LocationName,
		AliasLocationName: l.AliasLocationName,
		TypeAbsen:         l.TypeAbsen,
		MainLocation:      l.MainLocation,
		Address1:          l.Address1,
		Address2:          l.Address2,
		Address3:          l.Address3,
		Reference:         l.Reference,
		Telp:              l.Telp,
		Latitude:          l.Latitude,
		Longitude:         l.Longitude,
		Radius:            l.Radius,
		TimeZoneID:        l.TimeZoneID,
		Notes:             l.Notes,
		CreatedBy:         l.CreatedBy,
		CreatedHost:       l.CreatedHost,
		CreatedTime:       l.CreatedTime,
		UpdatedBy:         l.UpdatedBy,
		UpdatedHost:       l.UpdatedHost,
		UpdatedTime:       l.UpdatedTime,
		ReasonDeleted:     l.ReasonDeleted,
		DeletedBy:         l.DeletedBy,
	}
}

func (l LocationCoordinates) ToDTO() *dto.LocationCoordinates {
	locationCoordinates := dto.LocationCoordinates{}
	d, err := json.Marshal(l)
	if err != nil {
		logger.Error(err.Error())
	}

	err = json.Unmarshal(d, &locationCoordinates)
	if err != nil {
		logger.Error(err.Error())
	}

	return &locationCoordinates
}
