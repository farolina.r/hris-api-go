package domain

import (
	"encoding/json"
	"gitlab.com/farolina.r/hris-api-go/dto"
	"gitlab.com/farolina.r/hris-api-go/logger"
	"gitlab.com/farolina.r/hris-api-go/resp"
	"time"
)

type MenuRepository interface {
	GetMenuMobile(staffCode string) (*DefaultMenuData, *resp.ErrorResponse)
	SetDefaultMenuMobile(staffCode, host string, request *DefaultEmployeeMenuMobile) *resp.ErrorResponse
}

type EmployeeMenuMobile struct {
	StaffCode     int64      `db:"StaffCode"`
	MenuID        int        `db:"MenuID"`
	SeqNo         *int32     `db:"SeqNo"`
	Notes         *string    `db:"Notes"`
	CreatedBy     string     `db:"CreatedBy"`
	CreatedTime   time.Time  `db:"CreatedTime"`
	CreatedHost   *string    `db:"CreatedHost"`
	UpdatedBy     *string    `db:"UpdatedBy"`
	UpdatedTime   *time.Time `db:"UpdatedTime"`
	UpdatedHost   *string    `db:"UpdatedHost"`
	DeletedBy     *string    `db:"DeletedBy"`
	ReasonDeleted *string    `db:"ReasonDeleted"`
}

type DefaultEmployeeMenuMobile struct {
	StaffCode int64     `json:"StaffCode"`
	MenuID    []int     `json:"MenuID"`
	SeqNo     []int32   `json:"SeqNo"`
	Notes     *[]string `json:"Notes"`
}

type MenuMobile struct {
	ID              int64  `db:"id"`
	ParentMenuID    int64  `db:"ParrentMenuID"`
	DisplayOrder    int32  `db:"DisplayOrder"`
	DefaultMenu     int    `db:"DefaultMenu"`
	MenuName        string `db:"MenuName"`
	MenuDescription string `db:"MenuDescription"`
	Icon            string `db:"Icon"`
	HeadFlag        int    `db:"HeadFlag"`
	Controller      string `db:"Controller"`
	MenuPath        string `db:"MenuPath"`
	ControllerChild string `db:"ControllerChild"`
	MenuLevel       int32  `db:"MenuLevel"`
}

type MenuTree struct {
	ID              int64       `db:"id"`
	ParentMenuID    int64       `db:"ParrentMenuID"`
	DisplayOrder    int32       `db:"DisplayOrder"`
	DefaultMenu     int         `db:"DefaultMenu"`
	MenuName        string      `db:"MenuName"`
	MenuDescription string      `db:"MenuDescription"`
	Icon            string      `db:"Icon"`
	HeadFlag        int         `db:"HeadFlag"`
	Controller      string      `db:"Controller"`
	MenuPath        string      `db:"MenuPath"`
	ControllerChild string      `db:"ControllerChild"`
	MenuLevel       int32       `db:"MenuLevel"`
	Child           []*MenuNode `json:"Child"`
}

type MenuNode struct {
	ID              int64         `db:"id"`
	ParentMenuID    int64         `db:"ParrentMenuID"`
	DisplayOrder    int32         `db:"DisplayOrder"`
	DefaultMenu     int           `db:"DefaultMenu"`
	MenuName        string        `db:"MenuName"`
	MenuDescription string        `db:"MenuDescription"`
	Icon            string        `db:"Icon"`
	HeadFlag        int           `db:"HeadFlag"`
	Controller      string        `db:"Controller"`
	MenuPath        string        `db:"MenuPath"`
	ControllerChild string        `db:"ControllerChild"`
	MenuLevel       int32         `db:"MenuLevel"`
	Child           []*MenuMobile `json:"Child"`
}

type DefaultMenuData struct {
	MenuMobile     []*MenuMobile `json:"MenuMobile"`
	MenuMobileTree []*MenuTree   `json:"MenuMobileTree"`
}

func (e EmployeeMenuMobile) ToDTO() *dto.EmployeeMenuMobile {
	employeeMenuMobile := dto.EmployeeMenuMobile{}
	d, err := json.Marshal(e)
	if err != nil {
		logger.Error(err.Error())
	}

	err = json.Unmarshal(d, &employeeMenuMobile)
	if err != nil {
		logger.Error(err.Error())
	}

	return &employeeMenuMobile
}

func (e MenuTree) ToDTO() *dto.MenuTree {
	menuTree := dto.MenuTree{}
	d, err := json.Marshal(e)
	if err != nil {
		logger.Error(err.Error())
	}

	err = json.Unmarshal(d, &menuTree)
	if err != nil {
		logger.Error(err.Error())
	}

	return &menuTree
}

func (e MenuMobile) ToDTO() *dto.MenuMobile {
	menuMobile := dto.MenuMobile{}
	d, err := json.Marshal(e)
	if err != nil {
		logger.Error(err.Error())
	}

	err = json.Unmarshal(d, &menuMobile)
	if err != nil {
		logger.Error(err.Error())
	}

	return &menuMobile
}
