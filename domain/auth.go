package domain

import (
	"encoding/json"
	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/farolina.r/hris-api-go/dto"
	"gitlab.com/farolina.r/hris-api-go/helper"
	"gitlab.com/farolina.r/hris-api-go/logger"
	"gitlab.com/farolina.r/hris-api-go/resp"
	"os"
	"time"
)

const ExpirationTime = time.Hour * 24
const RefreshExpirationTime = time.Hour * 24 * 3

const UserSystem = "system"

type AuthStore interface {
	RefreshToken(string, string, string) (*resp.APIResponse, *resp.ErrorResponse)
	GetKey(key string) string
	SetKey(key, value string) error
	DeleteKey(key string) error
	BuildKey(prev, next string) string
	GetRefreshTokenKey(string) string
}

// AuthRepository TODO: refactor, APIResponse should be handled in service package
type AuthRepository interface {
	Login(Login, string) (*LoginResponse, *resp.AppError)
	Logout(AppTokenSession) (*resp.APIResponse, *resp.ErrorResponse)
	InsertAppTokenSession(AppTokenSession) *resp.ErrorResponse
	GetUserSessionByStaffCodeAndJWTToken(int64, string) (*AppTokenSession, *resp.ErrorResponse)
	RefreshToken(string, string, string) (*RefreshAuth, *resp.ErrorResponse)
	RemoveUserSessionByJWTToken(string, string) (*resp.APIResponse, *resp.ErrorResponse)
}

type Login struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Token    string `json:"token"`
}

type LoginResponse struct {
	Name           string    `json:"name"`
	StatusCode     string    `json:"status_code"`
	BlackpineToken string    `json:"blackpine_token"`
	JWTToken       string    `json:"jwt_token"`
	RefreshToken   string    `json:"refresh_token"`
	TokenType      string    `json:"token_type"`
	ExpiresIn      time.Time `json:"expires_in"`
}

type LoginUserData struct {
	StaffCode int64     `json:"StaffCode"`
	MaxDevice int       `json:"MaxDevice"`
	FullName  string    `json:"Fullname"`
	ExpiresIn time.Time `json:"ExpiresIn"`
}

type AppTokenSession struct {
	StaffCode      int64     `db:"StaffCode"`
	UserCode       string    `db:"UserCode"`
	BlackpineToken string    `db:"api_token"`
	JWTToken       string    `db:"jwt_token"`
	BranchCode     *string   `db:"BranchCode"`
	ExpiredAt      time.Time `db:"ExpiredAt"`
	CreatedBy      string    `db:"CreatedBy"`
	CreatedHost    string    `db:"CreatedHost"`
}

type RefreshAuth struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

func (a AppTokenSession) ToDTO() *dto.AppTokenSession {
	appTokenSession := dto.AppTokenSession{}
	d, err := json.Marshal(a)
	if err != nil {
		logger.Error(err.Error())
	}

	err = json.Unmarshal(d, &appTokenSession)
	if err != nil {
		logger.Error(err.Error())
	}

	return &appTokenSession
}

func createTokenString(claims AuthTokenClaims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString([]byte(os.Getenv("AUTH_SECRET")))
	if err != nil {
		logger.Error(err.Error())
		return "", err
	}

	return tokenString, nil
}

func (l LoginUserData) GenerateToken() (string, error) {
	var claims AuthTokenClaims
	claims = l.claimsForUser(helper.AddTimeUTC(time.Now(), ExpirationTime))

	return createTokenString(claims)
}

func (l LoginUserData) GenerateRefreshToken() (string, error) {
	var claims AuthTokenClaims
	claims = l.claimsForUser(helper.AddTimeUTC(time.Now(), RefreshExpirationTime))

	return createTokenString(claims)
}

func (l LoginUserData) claimsForUser(expiry time.Time) AuthTokenClaims {
	return AuthTokenClaims{
		l.StaffCode,
		l.MaxDevice,
		jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(expiry),
		},
	}
}
