package resp

import (
	"errors"
	"net/http"
)

var ErrLoggedOut = errors.New("you have been logged out")
var ErrTokenExpired = errors.New("token expired")
var ErrTokenInvalid = errors.New("token invalid")
var ErrRefreshTokenInvalidRetryLogin = errors.New("refresh token invalid, please try to login again")

const ReqBodyEmpty = "Request body is empty"
const ReqBodyInvalid = "Request body is invalid"

type AppError struct {
	Message string `json:"message"`
	Code    int    `json:"code"`
}

func OtherError(err error) *ErrorResponse {
	return &ErrorResponse{
		Message: err.Error(),
		Code:    http.StatusInternalServerError,
	}
}

func NotFoundError() *ErrorResponse {
	return &ErrorResponse{
		Message: "Not found",
		Code:    http.StatusNotFound,
	}
}

func InternalError() *ErrorResponse {
	return &ErrorResponse{
		Message: "Internal error",
		Code:    http.StatusInternalServerError,
	}
}

func ValidationError(message string) *ErrorResponse {
	return &ErrorResponse{
		Message: message,
		Code:    http.StatusBadRequest,
	}
}

func ReqBodyEmptyError() *ErrorResponse {
	return &ErrorResponse{
		Message: ReqBodyEmpty,
		Code:    http.StatusBadRequest,
	}
}

func ReqBodyInvalidError() *ErrorResponse {
	return &ErrorResponse{
		Message: ReqBodyInvalid,
		Code:    http.StatusBadRequest,
	}
}

func UnauthorizedError(message string) *ErrorResponse {
	if message == "" {
		message = "Unauthorized"
	}
	return &ErrorResponse{
		Message: message,
		Code:    http.StatusUnauthorized,
	}
}

func TokenExpiredError() *ErrorResponse {
	return &ErrorResponse{
		Message: "Token expired",
		Code:    http.StatusUnauthorized,
	}
}

func ForbiddenError(message string) *ErrorResponse {
	if message == "" {
		message = "Forbidden"
	}
	return &ErrorResponse{
		Message: message,
		Code:    http.StatusForbidden,
	}
}

func ResponseStatusError(code int, message string) *ErrorResponse {
	switch code {
	case http.StatusNotFound:
		return NotFoundError()
	case http.StatusInternalServerError:
		return InternalError()
	case http.StatusBadRequest:
		return ValidationError("Bad request")
	case http.StatusUnauthorized:
		return UnauthorizedError("")
	case http.StatusForbidden:
		return ForbiddenError(message)
	default:
		return &ErrorResponse{
			Message: "Unknown error",
			Code:    code,
		}
	}
}

func ConnectionError() *ErrorResponse {
	return &ErrorResponse{
		Message: "Connection error",
		Code:    http.StatusRequestTimeout,
	}
}
