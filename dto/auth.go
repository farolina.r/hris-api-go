package dto

import (
	"gitlab.com/farolina.r/hris-api-go/resp"
	"time"
)

type LoginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Token    string `json:"token"`
}

type AppTokenSession struct {
	StaffCode      int64     `json:"StaffCode"`
	BlackpineToken string    `json:"BlackpineToken"`
	JWTToken       string    `json:"JWTToken"`
	BranchCode     string    `json:"BranchCode"`
	ExpiredAt      time.Time `json:"ExpiredAt"`
	CreatedBy      string    `json:"CreatedBy"`
	CreatedHost    string    `json:"CreatedHost"`
}

type RefreshTokenRequest struct {
	RefreshToken string `json:"refresh_token"`
}

func (lr *LoginRequest) Validate() *resp.ErrorResponse {
	if lr.Username == "" {
		return resp.ValidationError("Username is required")
	}

	if lr.Password == "" {
		return resp.ValidationError("Password is required")
	}

	return nil
}

func (lr *RefreshTokenRequest) Validate() *resp.ErrorResponse {
	if lr.RefreshToken == "" {
		return resp.ValidationError("refresh_token is required")
	}

	return nil
}
