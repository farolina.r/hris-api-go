package dto

import (
	"gitlab.com/farolina.r/hris-api-go/resp"
)

type MenuMobile struct {
	ID              int64  `json:"id"`
	ParentMenuID    int64  `json:"ParrentMenuID"`
	DisplayOrder    int32  `json:"DisplayOrder"`
	DefaultMenu     int    `json:"DefaultMenu"`
	MenuName        string `json:"MenuName"`
	MenuDescription string `json:"MenuDescription"`
	Icon            string `json:"Icon"`
	HeadFlag        int    `json:"HeadFlag"`
	Controller      string `json:"Controller"`
	MenuPath        string `json:"MenuPath"`
	ControllerChild string `json:"ControllerChild"`
	MenuLevel       int32  `json:"MenuLevel"`
}

type DefaultEmployeeMenuMobile struct {
	StaffCode int64     `json:"StaffCode"`
	MenuID    []int     `json:"MenuID"`
	SeqNo     []int32   `json:"SeqNo"`
	Notes     *[]string `json:"Notes"`
}

type MenuTree struct {
	ID              int64       `json:"id"`
	ParentMenuID    int64       `json:"ParrentMenuID"`
	DisplayOrder    int32       `json:"DisplayOrder"`
	DefaultMenu     int         `json:"DefaultMenu"`
	MenuName        string      `json:"MenuName"`
	MenuDescription string      `json:"MenuDescription"`
	Icon            string      `json:"Icon"`
	HeadFlag        int         `json:"HeadFlag"`
	Controller      string      `json:"Controller"`
	MenuPath        string      `json:"MenuPath"`
	ControllerChild string      `json:"ControllerChild"`
	MenuLevel       int32       `json:"MenuLevel"`
	Child           []*MenuNode `json:"Child"`
}

type MenuNode struct {
	ID              int64         `json:"id"`
	ParentMenuID    int64         `json:"ParrentMenuID"`
	DisplayOrder    int32         `json:"DisplayOrder"`
	DefaultMenu     int           `json:"DefaultMenu"`
	MenuName        string        `json:"MenuName"`
	MenuDescription string        `json:"MenuDescription"`
	Icon            string        `json:"Icon"`
	HeadFlag        int           `json:"HeadFlag"`
	Controller      string        `json:"Controller"`
	MenuPath        string        `json:"MenuPath"`
	ControllerChild string        `json:"ControllerChild"`
	MenuLevel       int32         `json:"MenuLevel"`
	Child           []*MenuMobile `json:"Child"`
}

type DefaultMenuData struct {
	MenuMobile     []*MenuMobile `json:"MenuMobile"`
	MenuMobileTree []*MenuTree   `json:"MenuMobileTree"`
}

func (m *DefaultEmployeeMenuMobile) Validate() *resp.ErrorResponse {
	if m.StaffCode == 0 {
		return resp.ValidationError("StaffCode is required")
	}

	if m.MenuID == nil || len(m.MenuID) == 0 {
		return resp.ValidationError("MenuID is required")
	}

	if m.SeqNo == nil || len(m.SeqNo) == 0 {
		return resp.ValidationError("SeqNo is required")
	}

	return nil
}
