package dto

import (
	"time"
)

type EmployeeAuthRequest struct {
	GrantType string `json:"grant_type"`
	Username  string `json:"username"`
	Password  string `json:"password"`
}

type Employee struct {
	ID        string `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Email     string `json:"email"`
	Password  string `json:"password"`
}

type EmployeePersonalData struct {
	StaffCode          int64      `db:"StaffCode"`
	PIN                *string    `db:"PIN"`
	MaxDevice          int        `db:"MaxDevice"`
	NIK                string     `db:"NIK"`
	CardID             *string    `db:"CardID"`
	BuildingCardId     *string    `db:"BuildingCardId"`
	AbsenType          *string    `db:"AbsenType"`
	IdNumber           string     `db:"IdNumber"`
	NoKK               *string    `db:"NoKK"`
	FullName           string     `db:"Fullname"`
	ShortName          *string    `db:"ShortName"`
	SignInDate         time.Time  `db:"SignInDate"`
	ResignDate         *time.Time `db:"ResignDate"`
	FailedJoinDate     *time.Time `db:"FailedJoinDate"`
	StatusEmployeeCode int32      `db:"StatusEmployeeCode"`
	BirthPlace         *string    `db:"BirthPlace"`
	BirthDate          *time.Time `db:"BirthDate"`
	Phone              *string    `db:"Phone"`
	HandPhone          *string    `db:"HandPhone"`
	Handphone2         *string    `db:"Handphone2"`
	Email              *string    `db:"Email"`
	Email2             *string    `db:"Email2"`
	ChatID             *string    `db:"ChatID"`
	Address            *string    `db:"Address"`
	City               *string    `db:"City"`
	ZipCode            *string    `db:"ZipCode"`
	AddressDomisili    *string    `db:"AddressDomisili"`
	CityDomisili       *string    `db:"CityDomisili"`
	ZipCodeDomisili    *string    `db:"ZipCodeDomisili"`
	Extension          *string    `db:"Extension"`
	Gender             string     `db:"Gender"`
	Religion           *string    `db:"Religion"`
	PassportNo         *string    `db:"PassportNo"`
	BloodType          *string    `db:"BloodType"`
	BankAccountNo      *string    `db:"BankAccountNo"`
	NPWPNo             *string    `db:"NPWPNo"`
	NPWPDate           *time.Time `db:"NPWPDate"`
	LastNIK            *string    `db:"LastNIK"`
	CustomerIDCBN      *string    `db:"CustomerIDCBN"`
	ProfilPict         *string    `db:"ProfilPict"`
	IdCardPict         *string    `db:"IdCardPict"`
	FamilyCardPict     *string    `db:"FamilyCardPict"`
	VaccineCardPict1   *string    `db:"VaccineCardPict1"`
	VaccineCardPict2   *string    `db:"VaccineCardPict2"`
	DeviceID1          *string    `db:"DeviceID1"`
	DeviceID2          *string    `db:"DeviceID2"`
	Notes              *string    `db:"Notes"`
	CreatedBy          string     `db:"CreatedBy"`
	CreatedTime        *time.Time `db:"CreatedTime"`
	CreatedHost        string     `db:"CreatedHost"`
	UpdatedBy          *string    `db:"UpdatedBy"`
	UpdatedTime        *time.Time `db:"UpdatedTime"`
	UpdatedHost        *string    `db:"UpdatedHost"`
	DeletedBy          *string    `db:"DeletedBy"`
	ReasonDeleted      *string    `db:"ReasonDeleted"`
	Division           string     `db:"Division"`
	DivisionAlias      *string    `db:"DivisionAlias"`
	Department         *string    `db:"Department"`
	SubDepartment      *string    `db:"SubDepartment"`
	Group              *string    `db:"Group"`
	StaffStatusName    string     `db:"StaffStatusName"`
	GenderName         string     `db:"GenderName"`
	ReligionName       string     `db:"ReligionName"`
	JobFunctionName    *string    `db:"JobFunctionName"`
	PositionName       *string    `db:"PositionName"`
	PositionCode       string     `db:"PositionCode"`
	ImageBase          *string    `db:"ImageBase"`
}

type EmployeeMenuMobile struct {
	StaffCode     int64     `json:"StaffCode"`
	MenuID        int       `json:"MenuID"`
	SeqNo         int       `json:"SeqNo"`
	Notes         string    `json:"Notes"`
	CreatedBy     string    `json:"CreatedBy"`
	CreatedTime   time.Time `json:"CreatedTime"`
	CreatedHost   string    `json:"CreatedHost"`
	UpdatedBy     string    `json:"UpdatedBy"`
	UpdatedTime   time.Time `json:"UpdatedTime"`
	UpdatedHost   string    `json:"UpdatedHost"`
	DeletedBy     string    `json:"DeletedBy"`
	ReasonDeleted string    `json:"ReasonDeleted"`
}

type EmployeeSchedule struct {
}

type EmployeeFamily struct {
	ID              int64     `json:"id"`
	StaffCode       int64     `json:"StaffCode"`
	SeqNo           int       `json:"SeqNo"`
	TypeFamily      int32     `json:"TypeFamily"`
	Fullname        string    `json:"Fullname"`
	IdNumber        string    `json:"IdNumber"`
	Gender          string    `json:"Gender"`
	BloodType       string    `json:"BloodType"`
	BirthPlace      string    `json:"BirthPlace"`
	HandPhone       string    `json:"HandPhone"`
	Claim           int32     `json:"Claim"`
	CertificateNo   string    `json:"CertificateNo"`
	CertificateDate time.Time `json:"CertificateDate"`
	Occupation      string    `json:"Occupation"`
	EducationCode   int32     `json:"EducationCode"`
	MotherName      string    `json:"MotherName"`
	Citizenship     string    `json:"Citizenship"`
	NPWPNo          string    `json:"NPWPNo"`
	CreatedBy       string    `json:"CreatedBy"`
	CreatedTime     time.Time `json:"CreatedTime"`
	CreatedHost     string    `json:"CreatedHost"`
	UpdatedBy       string    `json:"UpdatedBy"`
	UpdatedTime     time.Time `json:"UpdatedTime"`
	UpdatedHost     string    `json:"UpdatedHost"`
	DeletedBy       string    `json:"DeletedBy"`
	ReasonDeleted   string    `json:"ReasonDeleted"`
}

type EmployeeSecondaryJob struct {
	ID              int64  `json:"id"`
	Division        string `json:"Division"`
	DivisionAlias   string `json:"DivisionAlias"`
	Department      string `json:"Department"`
	SubDepartment   string `json:"SubDepartment"`
	JobFunctionName string `json:"JobFunctionName"`
	PositionName    string `json:"PositionName"`
	PositionCode    string `json:"PositionCode"`
}

type EmployeePersonalInfo struct {
	Employee             []*EmployeePersonalData `json:"Employee"`
	DefaultMenu          []*EmployeeMenuMobile   `json:"DefaultMenu"`
	Schedule             []*EmployeeSchedule     `json:"Schedule"`
	EmployeeFamily       []*EmployeeFamily       `json:"EmployeeFamily"`
	EmployeeSecondaryJob []*EmployeeSecondaryJob `json:"EmployeeSecondaryJob"`
}

type EmployeeStructure struct {
	ID                int64   `json:"-"`
	StaffCode         int64   `json:"StaffCode"`
	Company           int32   `json:"Company"`
	DivisionCode      *int32  `json:"DivisionCode"`
	DepartmentCode    *int32  `json:"DepartmentCode"`
	SubDepartmentCode *int32  `json:"SubDepartmentCode"`
	GroupCode         *int32  `json:"GroupCode"`
	LevelCode         *string `json:"LevelCode"`
	JobFunctionCode   *int32  `json:"JobFunctionCode"`
	PositionCode      string  `json:"PositionCode"`
}
