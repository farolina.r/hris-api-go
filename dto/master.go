package dto

import (
	"gitlab.com/farolina.r/hris-api-go/resp"
	"time"
)

type LocationCoordinates struct {
	Latitude  *float64 `json:"latitude"`
	Longitude *float64 `json:"longitude"`
}

type LocationAccess struct {
	ID                int32      `json:"id"`
	IDIntranet        *string    `json:"id_intranet"`
	Ip                *string    `json:"ip"`
	Port              *string    `json:"port"`
	LocationName      string     `json:"location_name"`
	AliasLocationName *string    `json:"alias_location_name"`
	TypeAbsen         int8       `json:"type_absen"`
	MainLocation      *string    `json:"main_location"`
	Address1          *string    `json:"address1"`
	Address2          *string    `json:"address2"`
	Address3          *string    `json:"address3"`
	Reference         *string    `json:"reference"`
	Telp              *string    `json:"telp"`
	Latitude          *float64   `json:"latitude"`
	Longitude         *float64   `json:"longitude"`
	Radius            *int       `json:"radius"`
	TimeZoneID        *string    `json:"time_zone_id"`
	Notes             *string    `json:"notes,omitempty"`
	CreatedBy         string     `json:"created_by,omitempty"`
	CreatedHost       string     `json:"created_host,omitempty"`
	CreatedTime       time.Time  `json:"created_time,omitempty"`
	UpdatedBy         *string    `json:"updated_by,omitempty"`
	UpdatedHost       *string    `json:"updated_host,omitempty"`
	UpdatedTime       *time.Time `json:"updated_time,omitempty"`
	DeletedBy         *string    `json:"deleted_by,omitempty"`
	ReasonDeleted     *string    `json:"reason_deleted,omitempty"`
	DistanceMeters    *float64   `json:"distance_meters"`
}

type AttendanceLocationResponse struct {
	ListLocation []*LocationAccess `json:"ListLocation"`
}

func (lc *LocationCoordinates) Validate() *resp.ErrorResponse {
	if lc.Latitude == nil {
		return resp.ValidationError("latitude is required")
	}

	if lc.Longitude == nil {
		return resp.ValidationError("longitude is required")
	}

	return nil
}
