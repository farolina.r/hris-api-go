package dto

import (
	"gitlab.com/farolina.r/hris-api-go/resp"
)

type DevicePINRequest struct {
	DeviceID    *string `json:"DeviceID"`
	PIN         *string `json:"PIN"`
	Token       *string `json:"Token"`
	ModelDevice *string `json:"ModelDevice"`
	OSDevice    *string `json:"OSDevice"`
}

type EmployeeDevice struct {
	StaffCode int64  `json:"StaffCode"`
	DeviceID  string `json:"DeviceID"`
}

type RemoveDeviceRequest struct {
	StaffCode     int64   `json:"StaffCode"`
	DeviceID      string  `json:"DeviceID"`
	ReasonDeleted *string `json:"ReasonDeleted"`
}

type EmployeesPersonalInfoRequest struct {
	SearchKey string `json:"SearchKey"`
}

func (lr *DevicePINRequest) Validate() *resp.ErrorResponse {
	if lr.DeviceID == nil {
		return resp.ValidationError("DeviceID is required")
	}

	if lr.PIN == nil {
		return resp.ValidationError("PIN is required")
	}

	if lr.Token == nil {
		return resp.ValidationError("Token is required")
	}

	return nil
}

func (lr *RemoveDeviceRequest) Validate() *resp.ErrorResponse {
	if lr.DeviceID == "" {
		return resp.ValidationError("DeviceID is required")
	}

	if lr.StaffCode == 0 {
		return resp.ValidationError("StaffCode is required")
	}

	return nil
}
