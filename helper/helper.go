package helper

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/jmoiron/sqlx"
	"gitlab.com/farolina.r/hris-api-go/dto"
	"gitlab.com/farolina.r/hris-api-go/logger"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"strings"
	"time"
)

func Getenv(key, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	return value
}

func BuildSQL(dataset exp.SQLExpression) string {
	str, _, err := dataset.ToSQL()
	str = SanitizeSQL(str)

	if err != nil {
		logger.Error(err.Error())
	}

	return str
}

func SanitizeSQL(query string) string {
	return strings.ReplaceAll(query, "\"", "`")
}

// GetIP gets a requests IP address by reading off the forwarded-for
// header (for proxies) and falls back to use the remote address.
func GetIP(req *http.Request) string {
	ip, _, err := net.SplitHostPort(req.RemoteAddr)
	if err != nil {
		logger.Error(err.Error())
		return ""
	}

	return ip
}

func DecodeReqBody(w http.ResponseWriter, r *http.Request, target interface{}) error {
	err := json.NewDecoder(r.Body).Decode(&target)
	if err != nil {
		if err != io.EOF {
			logger.Error(err.Error())
		}
	}

	return err
}

func ExecQuery(client *sqlx.DB, query string) error {
	tx, err := client.Beginx()
	if err != nil {
		logger.Error(err.Error())
		return err
	}
	_, err = tx.Exec(query)
	if err != nil {
		logger.Error(err.Error())
		_ = tx.Rollback()
		return err
	}

	err = tx.Commit()
	if err != nil {
		logger.Error(err.Error())
		_ = tx.Rollback()
		return err
	}

	return nil
}

func GetAppTokenSessionFromContext(r *http.Request) (*dto.AppTokenSession, error) {
	user := r.Context().Value("user")
	if user == nil {
		return nil, errors.New("user not in context")
	}
	// get staffcode from user
	var session *dto.AppTokenSession
	if u, ok := user.(*dto.AppTokenSession); ok {
		session = u
	} else {
		logger.Error("error casting user to AppTokenSession")
		return nil, errors.New("context is not of type domain.AppTokenSession")
	}

	return session, nil
}

func GetStaffCodeFromContext(r *http.Request) (int64, error) {
	user := r.Context().Value("user")
	if user == nil {
		return 0, errors.New("user not in context")
	}
	// get staffcode from user
	var staffCode int64
	if u, ok := user.(*dto.AppTokenSession); ok {
		staffCode = u.StaffCode
	} else {
		logger.Error("error casting user to AppTokenSession")
		return 0, errors.New("context is not of type dto.AppTokenSession")
	}
	return staffCode, nil
}

func GetCurrentTimeUTC() string {
	return time.Now().UTC().Format("2006-01-02 03:04:05")
}

func AddTimeUTC(t time.Time, d time.Duration) time.Time {
	return t.Add(d).UTC()
}

func ConvertTimeUTCToString(t time.Time) string {
	return t.UTC().Format("2006-01-02 03:04:05")
}

func ConvertUnixUTCToString(timestamp int64) string {
	return time.Unix(timestamp, 0).UTC().Format("2006-01-02 03:04:05")
}

func toBase64(data []byte) string {
	return base64.StdEncoding.EncodeToString(data)
}

func GetImageBase64FromURL(url string) (string, error) {
	resp, err := http.Get(url)
	if err != nil {
		logger.Error(err.Error())
		return "", err
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			logger.Error(err.Error())
		}
	}(resp.Body)

	if resp.StatusCode != 200 {
		return "", errors.New("image not found")
	}

	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	var base64Encoding string
	mimeType := http.DetectContentType(bytes)

	switch mimeType {
	case "image/jpeg":
		base64Encoding += "data:image/jpeg;base64,"
	case "image/png":
		base64Encoding += "data:image/png;base64,"
	}

	base64Encoding += toBase64(bytes)

	return base64Encoding, nil
}

func PointerStringToString(s *string) string {
	if s == nil {
		return ""
	}
	return *s
}
