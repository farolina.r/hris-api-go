FROM golang:latest

ENV GO111MODULE=on
ENV PORT=8000
WORKDIR /app

COPY go.mod .
COPY go.sum .


RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build

EXPOSE 8000

CMD [ "/app/hris-api-go" ]
