package main

import (
	"github.com/joho/godotenv"
	"gitlab.com/farolina.r/hris-api-go/app"
	"gitlab.com/farolina.r/hris-api-go/logger"
)

func main() {
	// load env with go-dotenv
	err := godotenv.Load()
	if err != nil {
		logger.Fatal("Error loading .env file")
		panic(err)
	}

	app.Start()
}
