package store

import (
	"fmt"
	"github.com/go-redis/redis"
	"gitlab.com/farolina.r/hris-api-go/domain"
	"gitlab.com/farolina.r/hris-api-go/helper"
	"gitlab.com/farolina.r/hris-api-go/logger"
	"gitlab.com/farolina.r/hris-api-go/resp"
	"net/http"
)

const (
	// storeKeyAuth is the key for the auth store
	storeKeyAuth             = "auth"
	storeKeyAuthToken        = "auth:token"
	storeKeyAuthRefreshToken = "refresh"
	storeKeyAuthAccessToken  = "access"
)

type AuthStoreDefault struct {
	client *redis.Client
	repo   domain.AuthRepository
}

func (a AuthStoreDefault) RefreshToken(accessToken string, refreshToken string, host string) (*resp.APIResponse, *resp.ErrorResponse) {
	refreshKey := a.GetRefreshTokenKey(accessToken)

	token := a.GetKey(refreshKey)
	if token != "" && token != refreshToken {
		_ = a.DeleteKey(refreshKey)

		_, appErr := a.repo.RemoveUserSessionByJWTToken(accessToken, host)
		if appErr != nil {
			logger.Error(appErr.Message)
		}
		return nil, resp.ForbiddenError(resp.ErrRefreshTokenInvalidRetryLogin.Error())
	}

	tokens, appErr := a.repo.RefreshToken(accessToken, refreshToken, host)
	if appErr != nil {
		return nil, appErr
	}

	// remove old token
	_ = a.DeleteKey(refreshKey)

	// set tokens to store
	refreshKey = a.GetRefreshTokenKey(tokens.AccessToken)

	_ = a.SetKey(refreshKey, tokens.RefreshToken)

	response := resp.NewAPIResponse(http.StatusOK, true, "", tokens)

	return response, nil
}

func (a AuthStoreDefault) GetRefreshTokenKey(accessToken string) string {
	// get token from store by access token if exists
	// KEY: auth:token:access:<AccessToken>:refresh:<RefreshToken>
	accessKey := fmt.Sprintf("%s:%s", storeKeyAuthToken, storeKeyAuthAccessToken)
	refreshKey := fmt.Sprintf("%s:%s:%s", accessKey, accessToken, storeKeyAuthRefreshToken)

	return refreshKey
}

func (a AuthStoreDefault) DeleteKey(prefix string) error {
	err := helper.DeleteKey(a.client, prefix)
	if err != nil {
		return err
	}
	return nil
}

func (a AuthStoreDefault) GetKey(key string) string {
	val := helper.GetKey(a.client, key)

	return val
}

func (a AuthStoreDefault) SetKey(key, value string) error {
	err := helper.SetKey(a.client, key, value)
	if err != nil {
		logger.Error(err.Error())
		return err
	}
	return nil
}

func (a AuthStoreDefault) BuildKey(prev string, next string) string {
	// get last charater of prev
	val := helper.BuildKey(a.client, prev, next)

	return val
}

func NewAuthStore(client *redis.Client, authRepo domain.AuthRepository) domain.AuthStore {
	return &AuthStoreDefault{
		client: client,
		repo:   authRepo,
	}
}
