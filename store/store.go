package store

import (
	"fmt"
	"github.com/go-redis/redis"
	"gitlab.com/farolina.r/hris-api-go/logger"
)

//type Store interface {
//	GetKey(key string) string
//	SetKey(key, value string) error
//	DeleteKey(key string) error
//	BuildKey(prev, next string) string
//}

func DeleteKey(client *redis.Client, prefix string) error {
	iter := client.Scan(0, prefix, 0).Iterator()
	for iter.Next() {
		err := client.Del(iter.Val()).Err()
		if err != nil {
			logger.Error(err.Error())
			return err
		}
	}
	if err := iter.Err(); err != nil {
		logger.Error(err.Error())
		return err
	}

	return nil
}

func GetKey(client *redis.Client, key string) string {
	val, err := client.Get(key).Result()
	if err != nil {
		logger.Error(err.Error())
		return ""
	}

	return val
}

func SetKey(client *redis.Client, key, value string) error {
	err := client.Set(key, value, 0).Err()
	if err != nil {
		logger.Error(err.Error())
		return err
	}
	return nil
}

func BuildKey(client *redis.Client, prev string, next string) string {
	// get last charater of prev
	lastChar := prev[len(prev)-1:]
	if lastChar == ":" {
		return fmt.Sprintf("%s%s", prev, next)
	}
	return fmt.Sprintf("%s:%s", prev, next)
}
