include .env
DB_URI:=$(DB_CONNECTION)://$(DB_USERNAME):$(DB_PASSWORD)@tcp($(DB_HOST):$(DB_PORT))/$(DB_DATABASE)?parseTime=true

migrateup:
	migrate -path db/migrations -database "$(DB_URI)" -verbose up

migratedown:
	migrate -path db/migrations -database "$(DB_URI)" -verbose down

.PHONY: migrateup migratedown
